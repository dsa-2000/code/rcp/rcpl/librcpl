# librcpl

Common library for projects in the
[rcpl](https://gitlab.com/dsa-2000/rcp/rcpl) group: a collection
of useful miscellaneous functions (*e.g*,
[bark](https://gitlab.com/dsa-2000/code/common/bark) logging interface
to [Legion](https://legion.stanford.edu) logging); definitions of the
data packets sent from the radio camera front-end to the radio camera
processor, and of a logical region to represent sample data; and some
utility tasks.

`librcpl` also provides a transitive dependency on
[Legion](https://legion.stanford.edu) in
[rcpl](https://gitlab.com/dsa-2000/rcp/rcpl). As is, the
configuration of [Legion](https://legion.stanford.edu) determines the
value of a number of macros in `librcpl` that may be used by client
codes to include or exclude various kernel implementations to be
built.

## Build guidance

- Minimum [CMake](https://cmake.org) version: 3.23
- C++ standard: 20
  - tested with [gcc](https://gcc.gnu.org) v12.1.0
- Dependencies
  - [bark](https://gitlab.com/dsa-2000/rcp/bark), minimum
    version 0.103.0
  - [librcp](https://gitlab.com/dsa-2000/rcp/librcp), minimum
    version 0.104.0
  - [rcpdc](https://gitlab.com/dsa-2000/rcp/rcpdc), minimum
    version 0.104.0
  - [Legion](https://legion.stanford.edu), control replication branch
  - [Kokkos](https://github.com/kokkos/kokkos), minimum v4.00.01
    (indirect dependency, *via* Legion, but minimum version is
    required)

It is important that client code projects that are built with
[CMake](https://cmake.org) and use
[Kokkos](https://github.com/kokkos/kokkos) rely exclusively on the
imported `Kokkos::kokkoscore` and `Kokkos::kokkoscontainers` targets
provided by a call to `find_package(librcpl)`. These targets are
modified relative to the original
[Kokkos](https://github.com/kokkos/kokkos) versions for correct
interoperability with [Legion](https://legion.stanford.edu) and
[OpenMP](https://www.openmp.org).[^kokkos-targets]

[^kokkos-targets]: This is strictly necessary only when both CUDA and
OpenMP Kokkos backends are enabled, but there is no harm in using the
modified targets in other cases as well.

## Spack package

[Spack](https://spack.io) package is available in the DSA-2000 Spack
[package repository](https://gitlab.com/dsa-2000/code/spack).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/rcpl/librcpl. Instructions for cloning
the code repository can be found on that site. To provide feedback
(*e.g* bug reports or feature requests), please see the issue tracker
on that site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
