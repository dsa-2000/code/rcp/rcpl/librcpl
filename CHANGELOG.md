## v0.107.0
 (2024-01-30)

### New features

- Add support for standardized region definition, creation and access

### Bug fixes (1 change)

- [Fix coordinate types in create_partition_by_slicing()](dsa-2000/rcp/rcpl/librcpl@9eaa81619ecc25edff68223bdc56c4e3b6684659)

## v0.106.0
 (2023-11-21)

### Other (1 change)

- [Update dependent library versions](dsa-2000/rcp/rcpl/librcpl@db8c22b10788b87d01582fc45317a5ad8f0c2893)

## v0.105.0
 (2023-11-16)

### Bug fixes (1 change)

- [Add Legion dependency to librcpl unconditionally.](dsa-2000/code/rcp/rcpl/librcpl@53b4aef789b1402e5385354bdefcb48264fb0f0a)

### Feature changes (2 changes)

- [Change fsample_duration_t to have a signed integer representation](dsa-2000/code/rcp/rcpl/librcpl@0309842ecf61ebc61786974737aac6fe0da48253)
- [Use system_clock::time_point for time value type](dsa-2000/code/rcp/rcpl/librcpl@acb35b6b37e5ea2d6b7f4705fae0fca87c55b49a)

### Other (1 change)

- [Update rcpdc dependency to v0.103.0](dsa-2000/code/rcp/rcpl/librcpl@7d363d62a81c721a32c04b0ae75cb6fb220a4c7f)

## v0.104.0
 (2023-07-14)

### Feature changes (1 change)

- [Define RCPL_ and RCPLK_ macros for portable function declarations](dsa-2000/code/rcp/rcpl/librcpl@03ef9a2699c53f7a9ede7b531aed6a9174cf445e)

## v0.103.0
 (2023-07-02)

### Feature changes (2 changes)

- [Strip library name from log fields](dsa-2000/code/rcp/rcpl/librcpl@42a3aeb9939d1dbcaffa570a41baa4a445829f51)
- [Capitalize CMake target namespaces](dsa-2000/code/rcp/rcpl/librcpl@8ff9b1329434ff8fe96bfab620d987002ed5a386)

### Other (1 change)

- [Update bark, librcp, rcpdc dependency versions](dsa-2000/code/rcp/rcpl/librcpl@3003925050f857838a5d8920af643f9153bd21bb)

## v0.102.0
 (2023-06-16)

### New features (1 change)

- [Add libatomic to link dependencies of librcpl libraries](dsa-2000/code/rcp/rcpl/librcpl@a42606122827e4ada669c91a987b8c3fc038a8c9)

## v0.101.0
 (2023-06-14)

### Bug fixes (2 changes)

- [Fix contents of librcpl vs librcplk when CUDA is enabled](dsa-2000/code/rcp/rcpl/librcpl@22afc8c22bd97cb878e4835a4696fd0b8767a542)
- [Remove TCC dependency](dsa-2000/code/rcp/rcpl/librcpl@7d3d3a483b4cf088a0b8b82063b16764b2ae0655)

### Feature removals (1 change)

- [Remove {Librcpl,RCPL}_CAPTURE_BUFFER_TIMESTEPS_TILE_SIZE](dsa-2000/code/rcp/rcpl/librcpl@ddef250552eb18a03f44d1130acc5cbf481936bb)

### Other (1 change)

- [Set minimum Kokkos dependency version](dsa-2000/code/rcp/rcpl/librcpl@6e90d5f5dba45a3b12c5eb6e312fbf2058b5cffc)

## v0.100.0
 (2023-06-12)

### Other (1 change)

- Reset version number
