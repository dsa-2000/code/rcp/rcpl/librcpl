// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/rcpl/DataPacketConfiguration.hpp"

namespace std {
// DataPacketConfiguration stream insertion
//
// NOLINTBEGIN(cert-dcl58-cpp)
auto
operator<<(std::ostream& os, rcp::rcpl::DataPacketConfiguration const& /*dp*/)
  -> std::ostream& {

  using dpc_t = rcp::rcpl::DataPacketConfiguration;

  return os << "num_channels_per_packet [static]: "
            << dpc_t::num_channels_per_packet << '\n'
            << "num_timesteps_per_packet [static]: "
            << dpc_t::num_timesteps_per_packet << '\n'
            << "timesteps_tile_size [static]: " << dpc_t::timesteps_tile_size
            << '\n'
            << "num_polarizations [static]: " << dpc_t::num_polarizations
            << '\n'
            << "num_receivers [static]: " << dpc_t::num_receivers << '\n'
            << "fsample_interval [static]: "
            // << std::chrono::duration_cast<std::chrono::nanoseconds>(
            //   dc.fsample_interval) << '\n'
            << std::chrono::duration_cast<std::chrono::nanoseconds>(
                 dpc_t::fsample_interval)
                 .count()
            << " ns\n";
}
// NOLINTEND(cert-dcl58-cpp)
} // namespace std

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
