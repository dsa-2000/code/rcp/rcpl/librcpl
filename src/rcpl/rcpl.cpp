// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/rcpl/rcpl.hpp"

using namespace rcp::rcpl;

#if defined(RCPL_USE_KOKKOS) && defined(RCPL_USE_OPENMP)
// make up for a deficiency in the Realm OpenMP implementation that is needed by
// Kokkos. TODO: remove this when Realm implements it directly.
extern "C" {
int
omp_get_nested(void) {
  return 1;
}
}
#endif

// NOLINTBEGIN(cppcoreguidelines-avoid-non-const-global-variables)
LogSink<rcp::Tag> rcp::rcpl::log_rcp;
LogSink<Tag> rcp::rcpl::log_rcpl;
// NOLINTEND(cppcoreguidelines-avoid-non-const-global-variables)

template <>
auto
rcp::rcpl::get_log_sink<rcp::Tag>() -> LogSink<rcp::Tag>& {
  return log_rcp;
}

template <>
auto
rcp::rcpl::get_log_sink<Tag>() -> LogSink<Tag>& {
  return log_rcpl;
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
