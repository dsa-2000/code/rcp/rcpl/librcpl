// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/rcpl/Mapper.hpp"

// #include <mappers/logging_wrapper.h>

using namespace rcp::rcpl;

namespace rcp::rcpl {
/*! sharding functor for first-level of partition */
struct Level0ShardingFunctor : public L::ShardingFunctor {

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
  static L::ShardingID id;

  auto
  shard(
    L::DomainPoint const& point,
    L::Domain const& full_space,
    std::size_t total_shards) -> L::ShardID override;

  [[nodiscard]] auto
  is_invertible() const -> bool override;

  void
  invert(
    L::ShardID shard,
    L::Domain const& shard_domain,
    L::Domain const&
      full_domain, // NOLINT(bugprone-easily-swappable-parameters)
    std::size_t total_shards,
    std::vector<L::DomainPoint>& points) override;

  template <int DIM>
  static void
  gather_points(
    L::Domain const& full_domain,
    std::array<L::coord_t, 2> const& bnds,
    std::vector<L::DomainPoint>& points) {

    for (L::PointInDomainIterator<DIM> pid(full_domain); pid(); pid++)
      if (bnds[0] <= pid[0] && pid[0] <= bnds[1])
        points.push_back(*pid);
  }
};
} // namespace rcp::rcpl

auto
Level0ShardingFunctor::shard(
  L::DomainPoint const& point,
  L::Domain const& full_space,
  std::size_t total_shards) -> L::ShardID {

  std::size_t const pt = point[0];
  std::size_t const npt = full_space.hi()[0] + 1;
  auto const rem_pt = npt % total_shards;
  L::ShardID result{};
  if (rem_pt == 0) {
    result = pt / (npt / total_shards);
  } else {
    auto max_pt_per_shard = rcp::ceil_div(npt, total_shards);
    if (pt >= rem_pt * max_pt_per_shard)
      result =
        rem_pt + (pt - rem_pt * max_pt_per_shard) / (max_pt_per_shard - 1);
    else
      result = pt / max_pt_per_shard;
  }
  return result;
}

auto
Level0ShardingFunctor::is_invertible() const -> bool {
  return true;
}

void
Level0ShardingFunctor::invert(
  L::ShardID shard,
  L::Domain const& /*shard_domain*/,
  L::Domain const& full_domain, // NOLINT(bugprone-easily-swappable-parameters)
  std::size_t const total_shards,
  std::vector<L::DomainPoint>& points) {

  auto const npt = full_domain.hi()[0] + 1;
  auto const rem_pt = npt % L::coord_t(total_shards);
  auto const pt_per_shard = npt / L::coord_t(total_shards);
  std::array<L::coord_t, 2> bnds{};
  if (rem_pt == 0) {
    bnds[0] = shard * pt_per_shard;
    bnds[1] = bnds[0] + pt_per_shard - 1;
  } else {
    bnds[0] = shard * pt_per_shard + std::min(decltype(rem_pt){shard}, rem_pt);
    bnds[1] = bnds[0] + (pt_per_shard + ((shard < rem_pt) ? 1 : 0)) - 1;
  }
  switch (full_domain.get_dim()) {
    // NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BLOCK(DIM)                                                             \
  case DIM: {                                                                  \
    gather_points<DIM>(full_domain, bnds, points);                             \
    break;                                                                     \
  }
    LEGION_FOREACH_N (BLOCK)
#undef BLOCK
    default:
      assert(false);
  }
}

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
L::ShardingID Level0ShardingFunctor::id;

auto
Mapper::restricted_proc_kind(L::MappingTagID tag) -> L::Processor::Kind {
  auto val = ((tag >> proc_restricted_tag_shift)
              & ((1U << (next_tag_shift - proc_restricted_tag_shift)) - 1))
             - 1;
  switch (val) {
  case loc_restricted:
    return L::Processor::LOC_PROC;
  case omp_restricted:
    return L::Processor::OMP_PROC;
  case toc_restricted:
    return L::Processor::TOC_PROC;
  case py_restricted:
    return L::Processor::PY_PROC;
  case io_restricted:
    return L::Processor::IO_PROC;
  default:
    return L::Processor::NO_KIND;
  }
}

Mapper::Mapper(
  L::Machine const& machine,
  L::Runtime* rt,
  std::set<L::Processor> const& local_procs)
  : L::Mapping::DefaultMapper(
    rt->get_mapper_runtime(), machine, *(local_procs.begin())) {}

void
Mapper::select_task_options(
  L::Mapping::MapperContext const ctx,
  L::Task const& task,
  L::Mapping::Mapper::TaskOptions& options) {

  L::Mapping::DefaultMapper::select_task_options(ctx, task, options);

  // TODO: remove this permanently
  // for (auto&& idx : std::views::iota(std::size_t{0}, task.regions.size()))
  //   if ((task.regions[idx].tag & collective_instance_tag) != 0)
  //     options.check_collective_regions.insert(idx);
}

// NOLINTBEGIN(readability-function-cognitive-complexity)
void
Mapper::map_task(
  L::Mapping::MapperContext const ctx,
  L::Task const& task,
  L::Mapping::Mapper::MapTaskInput const& input,
  L::Mapping::Mapper::MapTaskOutput& output) {

  auto target_kind = task.target_proc.kind();
  // Get the variant that we are going to use to map this task
  auto chosen = default_find_preferred_variant(
    task, ctx, true /*needs tight bound*/, true /*cache*/, target_kind);
  output.chosen_variant = chosen.variant;
  if ((task.tag & high_priority_tag) != 0)
    output.task_priority = output.copy_fill_priority = high_priority_value;
  else
    output.task_priority = 0;
  output.postmap_task = false;
  // Figure out our target processors
  default_policy_select_target_processors(ctx, task, output.target_procs);
  auto target_proc = output.target_procs[0];
  // See if we have an inner variant, if we do virtually map all the regions
  // We don't even both caching these since they are so simple
  if (chosen.is_inner) {
    // Check to see if we have any relaxed coherence modes in which
    // case we can no longer do virtual mappings so we'll fall through
    bool has_relaxed_coherence = false;
    for (auto&& region : task.regions) {
      if (region.prop != LEGION_EXCLUSIVE) {
        has_relaxed_coherence = true;
        break;
      }
    }
    if (!has_relaxed_coherence) {
      std::vector<unsigned> reduction_indexes;
      for (unsigned idx = 0; idx < task.regions.size(); idx++) {
        // As long as this isn't a reduction-only region requirement
        // we will do a virtual mapping, for reduction-only instances
        // we will actually make a physical instance because the runtime
        // doesn't allow virtual mappings for reduction-only privileges
        if (task.regions[idx].privilege == LEGION_REDUCE)
          reduction_indexes.push_back(idx);
        else
          output.chosen_instances[idx].push_back(
            L::Mapping::PhysicalInstance::get_virtual_instance());
      }
      if (!reduction_indexes.empty()) {
        auto const& layout_constraints = runtime->find_task_layout_constraints(
          ctx, task.task_id, output.chosen_variant);
        for (auto&& rit : reduction_indexes) {
          auto mem_constraint =
            find_memory_constraint(ctx, task, output.chosen_variant, rit);
          auto target_memory = default_policy_select_target_memory(
            ctx, target_proc, task.regions[rit], mem_constraint);
          auto copy = task.regions[rit].privilege_fields;
          size_t footprint{};
          if (!create_custom_instances(
                ctx,
                target_proc,
                target_memory,
                task.regions[rit],
                rit,
                copy,
                layout_constraints,
                false /*needs constraint check*/,
                output.chosen_instances[rit],
                &footprint)) {
            default_report_failed_instance_creation(
              task, rit, target_proc, target_memory, footprint);
          }
        }
      }
      return;
    }
  }
  // Should we cache this task?
  auto cache_policy = default_policy_select_task_cache_policy(ctx, task);

  // First, let's see if we've cached a result of this task mapping
  auto const task_hash = compute_task_hash(task);
  std::pair<L::TaskID, L::Processor> const cache_key(task.task_id, target_proc);
  auto finder = cached_task_mappings.find(cache_key);
  // This flag says whether we need to recheck the field constraints,
  // possibly because a new field was allocated in a region, so our old
  // cached physical instance(s) is(are) no longer valid
  bool needs_field_constraint_check = false;
  if (
    cache_policy == DEFAULT_CACHE_POLICY_ENABLE
    && finder != cached_task_mappings.end()) {
    bool found = false;
    // Iterate through and see if we can find one with our variant and hash
    for (auto&& cached : finder->second) {
      if (
        (cached.variant == output.chosen_variant)
        && (cached.task_hash == task_hash)) {
        // Have to copy it before we do the external call which
        // might invalidate our iterator
        output.chosen_instances = cached.mapping;
        output.output_targets = cached.output_targets;
        output.output_constraints = cached.output_constraints;
        found = true;
        break;
      }
    }
    if (found) {
      // See if we can acquire these instances still
      if (runtime->acquire_and_filter_instances(ctx, output.chosen_instances))
        return;
      // We need to check the constraints here because we had a
      // prior mapping and it failed, which may be the result
      // of a change in the allocated fields of a field space
      needs_field_constraint_check = true;
      // If some of them were deleted, go back and remove this entry
      // Have to renew our iterators since they might have been
      // invalidated during the 'acquire_and_filter_instances' call
      default_remove_cached_task(
        ctx,
        output.chosen_variant,
        task_hash,
        cache_key,
        output.chosen_instances);
    }
  }
  // We didn't find a cached version of the mapping so we need to
  // do a full mapping, we already know what variant we want to use
  // so let's use one of the acceleration functions to figure out
  // which instances still need to be mapped.
  std::vector<std::set<L::FieldID>> missing_fields(task.regions.size());
  runtime->filter_instances(
    ctx, task, output.chosen_variant, output.chosen_instances, missing_fields);
  // Track which regions have already been mapped
  std::vector<bool> done_regions(task.regions.size(), false);
  if (!input.premapped_regions.empty())
    for (auto&& rg : input.premapped_regions)
      done_regions[rg] = true;
  auto const& layout_constraints = runtime->find_task_layout_constraints(
    ctx, task.task_id, output.chosen_variant);
  // Now we need to go through and make instances for any of our
  // regions which do not have space for certain fields
  for (unsigned idx = 0; idx < task.regions.size(); idx++) {
    if (done_regions[idx])
      continue;
    // Skip any empty regions
    if (
      (task.regions[idx].privilege == LEGION_NO_ACCESS)
      || (task.regions[idx].privilege_fields.empty())
      || missing_fields[idx].empty())
      continue;
    // See if this is a reduction
    auto mem_constraint =
      find_memory_constraint(ctx, task, output.chosen_variant, idx);
    auto target_memory = default_policy_select_target_memory(
      ctx, target_proc, task.regions[idx], mem_constraint);
    if (task.regions[idx].privilege == LEGION_REDUCE) {
      size_t footprint{};
      if (!create_custom_instances(
            ctx,
            target_proc,
            target_memory,
            task.regions[idx],
            idx,
            missing_fields[idx],
            layout_constraints,
            needs_field_constraint_check,
            output.chosen_instances[idx],
            &footprint)) {
        default_report_failed_instance_creation(
          task, idx, target_proc, target_memory, footprint);
      }
      continue;
    }
    // Did the application request a virtual mapping for this requirement?
    if ((task.regions[idx].tag & L::Mapping::DefaultMapper::VIRTUAL_MAP) != 0) {
      auto virt_inst = L::Mapping::PhysicalInstance::get_virtual_instance();
      output.chosen_instances[idx].push_back(virt_inst);
      continue;
    }
    // Check to see if any of the valid instances satisfy this requirement
    {
      std::vector<L::Mapping::PhysicalInstance> valid_instances;

      for (auto&& inst : input.valid_instances[idx]) {
        if (inst.get_location() == target_memory)
          valid_instances.push_back(inst);
      }

      std::set<L::FieldID> valid_missing_fields;
      runtime->filter_instances(
        ctx,
        task,
        idx,
        output.chosen_variant,
        valid_instances,
        valid_missing_fields);

#ifndef NDEBUG
      bool const check =
#endif
        runtime->acquire_and_filter_instances(ctx, valid_instances);
      assert(check);

      output.chosen_instances[idx] = valid_instances;
      missing_fields[idx] = valid_missing_fields;

      if (missing_fields[idx].empty())
        continue;
    }
    // Otherwise make normal instances for the given region
    size_t footprint{};
    if (!create_custom_instances(
          ctx,
          target_proc,
          target_memory,
          task.regions[idx],
          idx,
          missing_fields[idx],
          layout_constraints,
          needs_field_constraint_check,
          output.chosen_instances[idx],
          &footprint)) {
      default_report_failed_instance_creation(
        task, idx, target_proc, target_memory, footprint);
    }
  }

  // Finally we set a target memory for output instances
  auto target_memory =
    default_policy_select_output_target(ctx, task.target_proc);
  for (unsigned i = 0; i < task.output_regions.size(); ++i) {
    output.output_targets[i] = target_memory;
    default_policy_select_output_constraints(
      task, output.output_constraints[i], task.output_regions[i]);
  }

  if (cache_policy == DEFAULT_CACHE_POLICY_ENABLE) {
    // Now that we are done, let's cache the result so we can use it later
    auto& map_list = cached_task_mappings[cache_key];
    map_list.push_back(CachedTaskMapping());
    auto& cached_result = map_list.back();
    cached_result.task_hash = task_hash;
    cached_result.variant = output.chosen_variant;
    cached_result.mapping = output.chosen_instances;
    cached_result.output_targets = output.output_targets;
    cached_result.output_constraints = output.output_constraints;
  }
}
// NOLINTEND(readability-function-cognitive-complexity)

void
Mapper::select_sharding_functor(
  L::Mapping::MapperContext const ctx,
  L::Task const& task,
  L::Mapping::Mapper::SelectShardingFunctorInput const& input,
  L::Mapping::Mapper::SelectShardingFunctorOutput& output) {

  if ((task.tag & level0_sharding_tag) != 0) {
    output.chosen_functor = Level0ShardingFunctor::id;
    output.slice_recurse = false; // TODO: OK?
  } else {
    L::Mapping::DefaultMapper ::select_sharding_functor(
      ctx, task, input, output);
  }
}

auto
Mapper::default_policy_select_initial_processor(
  L::Mapping::MapperContext ctx, const L::Task& task) -> L::Processor {

  if (have_proc_kind_variant(ctx, task.task_id, L::Processor::PROC_SET)) {
    return default_get_next_global_procset();
  }

  VariantInfo info = default_find_preferred_variant(
    task, ctx, false /*needs tight*/, true, restricted_proc_kind(task.tag));
  //  are the right kind and this is an index space task launch
  // then we return ourselves
  if (task.is_index_space) {
    if (info.proc_kind == local_kind)
      return local_proc;
    // Otherwise round robin onto our local queue of the right kind
    switch (info.proc_kind) {
    case L::Processor::LOC_PROC:
      return default_get_next_local_cpu();
    case L::Processor::TOC_PROC:
      return default_get_next_local_gpu();
    case L::Processor::IO_PROC:
      return default_get_next_local_io();
    case L::Processor::OMP_PROC:
      return default_get_next_local_omp();
    case L::Processor::PY_PROC:
      return default_get_next_local_py();
    default: // make warnings go away
      break;
    }
    // should never get here
    assert(false);
    return L::Processor::NO_PROC;
  }
  // Do different things depending on our depth in the task tree
  const int depth = task.get_depth();
  switch (depth) {
  case 0: {
    // Top-level task: try to stay in place, otherwise choose
    // a suitable local processor.
    if (info.proc_kind == local_kind)
      return local_proc;
    switch (info.proc_kind) {
    case L::Processor::LOC_PROC:
      return default_get_next_local_cpu();
    case L::Processor::TOC_PROC:
      return default_get_next_local_gpu();
    case L::Processor::IO_PROC:
      return default_get_next_local_io();
    case L::Processor::OMP_PROC:
      return default_get_next_local_omp();
    case L::Processor::PY_PROC:
      return default_get_next_local_py();
    default: // make warnings go away
      break;
    }
  }
  case 1: {
    // First-level tasks: assume we should distribute these
    // evenly around the machine unless we've been explicitly
    // told not to
    // TODO: Fix this when we implement a good stealing algorithm
    // to instead encourage locality
    if (!same_address_space && (task.tag & SAME_ADDRESS_SPACE) == 0) {
      switch (info.proc_kind) {
      case L::Processor::LOC_PROC:
        return default_get_next_global_cpu();
      case L::Processor::TOC_PROC:
        return default_get_next_global_gpu();
      case L::Processor::IO_PROC: // Don't distribute I/O
        return default_get_next_local_io();
      case L::Processor::OMP_PROC:
        return default_get_next_global_omp();
      case L::Processor::PY_PROC:
        return default_get_next_global_py();
      default: // make warnings go away
        break;
      }
    }
    // fall through to local assignment code below
  }
  default: {
    // N-level tasks: assume we keep these local to our
    // current node as the distribution was done at level 1
    switch (info.proc_kind) {
    case L::Processor::LOC_PROC:
      return default_get_next_local_cpu();
    case L::Processor::TOC_PROC:
      return default_get_next_local_gpu();
    case L::Processor::IO_PROC:
      return default_get_next_local_io();
    case L::Processor::OMP_PROC:
      return default_get_next_local_omp();
    case L::Processor::PY_PROC:
      return default_get_next_local_py();
    default: // make warnings go away
      break;
    }
  }
  }
  // should never get here
  assert(false);
  return L::Processor::NO_PROC;
}

// NOLINTBEGIN(readability-function-cognitive-complexity)
auto
Mapper::create_custom_instances(
  L::Mapping::MapperContext ctx,
  L::Processor /*target_proc*/,
  L::Memory target_memory,
  L::RegionRequirement const& req,
  unsigned index,
  std::set<L::FieldID>& needed_fields,
  L::TaskLayoutConstraintSet const& layout_constraints,
  bool needs_field_constraint_check,
  std::vector<L::Mapping::PhysicalInstance>& instances,
  size_t* footprint) -> bool {

  // Special case for reduction instances, no point in checking
  // for existing ones and we also know that currently we can only
  // make a single instance for each field of a reduction
  if (req.privilege == LEGION_REDUCE) {
    // Iterate over the fields one by one for now, once Realm figures
    // out how to deal with reduction instances that contain
    bool force_new_instances = true; // always have to force new instances
    auto our_layout_id = default_policy_select_layout_constraints(
      ctx,
      target_memory,
      req,
      TASK_MAPPING,
      needs_field_constraint_check,
      force_new_instances);
    for (auto it = req.privilege_fields.begin();
         it != req.privilege_fields.end();
         it++) {
      for (auto lay_it = layout_constraints.layouts.lower_bound(index);
           lay_it != layout_constraints.layouts.upper_bound(index);
           lay_it++) {
        // Get the constraints
        auto const& index_constraints =
          runtime->find_layout_constraints(ctx, lay_it->second);
        auto const& constraint_fields =
          index_constraints.field_constraint.get_field_set();
        auto this_field =
          std::find(constraint_fields.begin(), constraint_fields.end(), *it);
        if (
          constraint_fields.empty() || this_field != constraint_fields.end()) {

          auto creation_constraints = index_constraints;
          default_policy_select_constraints(
            ctx, creation_constraints, target_memory, req);
          creation_constraints.field_constraint.field_set.clear();
          creation_constraints.field_constraint.field_set.push_back(*it);

          // Now figure out how to make an instance
          instances.resize(instances.size() + 1);

          // Check to see if these constraints conflict with our constraints
          // or whether they entail our mapper preferred constraints
          auto meets =
            constraint_fields.empty()
            || (
              !runtime
                ->do_constraints_conflict(ctx, our_layout_id, lay_it->second)
              && !runtime
                   ->do_constraints_entail(ctx, lay_it->second, our_layout_id));
          if (!default_make_instance(
                ctx,
                target_memory,
                creation_constraints,
                instances.back(),
                TASK_MAPPING,
                force_new_instances,
                meets,
                req,
                footprint))
            return false;
          break;
        }
      }
    }
    return true;
  }
  // Before we do anything else figure out our
  // constraints for any instances of this task, then we'll
  // see if these constraints conflict with or are satisfied by
  // any of the other constraints
  bool force_new_instances = false;
  auto our_layout_id = default_policy_select_layout_constraints(
    ctx,
    target_memory,
    req,
    TASK_MAPPING,
    needs_field_constraint_check,
    force_new_instances);
  auto const& our_constraints =
    runtime->find_layout_constraints(ctx, our_layout_id);
  for (auto lay_it = layout_constraints.layouts.lower_bound(index);
       lay_it != layout_constraints.layouts.upper_bound(index);
       lay_it++) {
    // Get the constraints
    auto const& index_constraints =
      runtime->find_layout_constraints(ctx, lay_it->second);
    std::vector<L::FieldID> overlapping_fields;
    auto const& constraint_fields =
      index_constraints.field_constraint.get_field_set();
    if (!constraint_fields.empty()) {
      for (auto&& fid : constraint_fields) {
        auto finder = needed_fields.find(fid);
        if (finder != needed_fields.end()) {
          overlapping_fields.push_back(fid);
          // Remove from the needed fields since we're going to handle it
          needed_fields.erase(finder);
        }
      }
      // If we don't have any overlapping fields, then keep going
      if (overlapping_fields.empty())
        continue;
    } else { // otherwise it applies to all the fields
      overlapping_fields.insert(
        overlapping_fields.end(), needed_fields.begin(), needed_fields.end());
      needed_fields.clear();
    }
    // Now figure out how to make an instance
    instances.resize(instances.size() + 1);
    // Check to see if these constraints conflict with our constraints
    // or whether they entail our mapper preferred constraints
    if (
      runtime->do_constraints_conflict(ctx, our_layout_id, lay_it->second)
      || runtime->do_constraints_entail(ctx, lay_it->second, our_layout_id)) {
      // They conflict or they entail our constraints so we're just going
      // to make an instance using these constraints
      // Check to see if they have fields and if not constraints with fields
      if (constraint_fields.empty()) {
        auto creation_constraints = index_constraints;
        default_policy_select_constraints(
          ctx, creation_constraints, target_memory, req);
        creation_constraints.add_constraint(L::FieldConstraint(
          overlapping_fields,
          index_constraints.field_constraint.contiguous,
          index_constraints.field_constraint.inorder));
        if (!default_make_instance(
              ctx,
              target_memory,
              creation_constraints,
              instances.back(),
              TASK_MAPPING,
              force_new_instances,
              true /*meets*/,
              req,
              footprint))
          return false;
      } else if (!default_make_instance(
                   ctx,
                   target_memory,
                   index_constraints,
                   instances.back(),
                   TASK_MAPPING,
                   force_new_instances,
                   false /*meets*/,
                   req,
                   footprint))
        return false;
    } else {
      // These constraints don't do as much as we want but don't
      // conflict so make an instance with them and our constraints
      auto creation_constraints = index_constraints;
      default_policy_select_constraints(
        ctx, creation_constraints, target_memory, req);
      creation_constraints.add_constraint(L::FieldConstraint(
        overlapping_fields,
        creation_constraints.field_constraint.contiguous
          || index_constraints.field_constraint.contiguous,
        creation_constraints.field_constraint.inorder
          || index_constraints.field_constraint.inorder));
      if (!default_make_instance(
            ctx,
            target_memory,
            creation_constraints,
            instances.back(),
            TASK_MAPPING,
            force_new_instances,
            true /*meets*/,
            req,
            footprint))
        return false;
    }
  }
  // If we don't have anymore needed fields, we are done
  if (needed_fields.empty())
    return true;
  // There are no constraints for these fields so we get to do what we want
  instances.resize(instances.size() + 1);
  auto creation_constraints = our_constraints;
  std::vector<L::FieldID> creation_fields;
  default_policy_select_instance_fields(
    ctx, req, needed_fields, creation_fields);
  creation_constraints.add_constraint(
    L::FieldConstraint(creation_fields, false /*contig*/, false /*inorder*/));
  return default_make_instance(
    ctx,
    target_memory,
    creation_constraints,
    instances.back(),
    TASK_MAPPING,
    force_new_instances,
    true /*meets*/,
    req,
    footprint);
}
// NOLINTEND(readability-function-cognitive-complexity)

void
Mapper::create_mappers(
  L::Machine machine, // NOLINT(performance-unnecessary-value-param)
  L::Runtime* rt,
  const std::set<L::Processor>& local_procs) {

  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  rt->replace_default_mapper(new Mapper(machine, rt, local_procs));
}

void
Mapper::preregister() {

  Level0ShardingFunctor::id = L::Runtime::generate_static_sharding_id();
  L::Runtime::preregister_sharding_functor(
    Level0ShardingFunctor::id,
    // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
    new Level0ShardingFunctor());
  L::Runtime::add_registration_callback(create_mappers);
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
