// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "DataPacketConfiguration.hpp"
#include "rcpl.hpp"

namespace rcp::rcpl {

/* \brief axes for f-engine sample regions */
enum class FSamplesAxes {
  channel = LEGION_DIM_0, /*!< channel index */
  receiver,               /*!< receiver/antenna index */
  polarization,           /*!< polarization */
  timestep                /*!< timestep */
};

/*! \brief IndexSpec specialization for FSamplesAxes
 *
 * \tparam Coord index coordinate value type
 */
template <typename Coord>
using fsamples_index_spec_t =
  IndexSpec<FSamplesAxes, FSamplesAxes::channel, FSamplesAxes::timestep, Coord>;

/*! \brief default static bounds for regions with FSamplesAxes
 *
 * \tparam Coord index coordinate value type
 *
 * Bounds are channel: [0, Coord max]; receiver: [0,
 * DataPacketConfiguration::num_receivers - 1]; polarization: [0,
 * DataPacketConfiguration::num_polarizations]; timestep: [0, Coord max]
 */
template <typename Coord>
constexpr auto default_fsamples_bounds = std::array{
  IndexInterval<Coord>::left_bounded(0),
  IndexInterval<Coord>::bounded(0, DataPacketConfiguration::num_receivers - 1),
  IndexInterval<Coord>::bounded(
    0, DataPacketConfiguration::num_polarizations - 1),
  IndexInterval<Coord>::left_bounded(0)};

/*! \brief field for f-engine sample values */
using FSampleField = StaticField<rcp::dc::fsample_type, 3>;

/*! \brief field for f-engine sample weight values */
using FWeightField = StaticField<rcp::dc::fweight_type, 4>;

/*! \brief FSamples region definition */
struct FSamplesRegion
  : public StaticRegionSpec<
      fsamples_index_spec_t<coord_t>,
      default_fsamples_bounds<coord_t>,
      FSampleField,
      FWeightField> {

  static constexpr FSampleField::value_t flagged{-8};
};

} // end namespace rcp::rcpl

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
