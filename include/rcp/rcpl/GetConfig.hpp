// Copyright 2022 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp/rcpl/rcpl.hpp"

#include <array>
#include <fstream>
#include <vector>

namespace rcp::rcpl {

/** read a configuration
 */
template <typename ConfigSerdez, L::TaskID TaskId>
class GetConfig_ {

  static constexpr auto task_id = TaskId;
  static constexpr char const* task_name = "GetConfig";

  struct proxy_configuration_t {
    std::array<char, ConfigSerdez::MAX_SERIALIZED_SIZE> buffer;
  };

  L::LogicalRegion m_lr;

  L::TaskLauncher m_launcher;

public:
  using Configuration = typename ConfigSerdez::FIELD_TYPE;

  GetConfig_(L::Context ctx, L::Runtime* rt) {

    auto is = rt->create_index_space(ctx, L::Rect<1, coord_t>{0, 0});
    auto fs = rt->create_field_space(ctx);
    auto fa = rt->create_field_allocator(ctx, fs);
    // we do our own serialization & deserialization, otherwise Legion emits
    // warnings about ignored field destructors
    fa.allocate_field(sizeof(proxy_configuration_t), 0);
    m_lr = rt->create_logical_region(ctx, is, fs);
    L::RegionRequirement req(
      m_lr, LEGION_WRITE_DISCARD, LEGION_EXCLUSIVE, m_lr);
    req.add_field(0);
    m_launcher = L::TaskLauncher(task_id, L::UntypedBuffer());
    m_launcher.add_region_requirement(req);
  }

  auto
  launch(L::Context ctx, L::Runtime* rt) -> Configuration {

    rt->execute_task(ctx, m_launcher).wait();
    L::RegionRequirement req(m_lr, LEGION_READ_ONLY, LEGION_EXCLUSIVE, m_lr);
    req.add_field(0);
    auto pr = rt->map_region(ctx, req);
    L::FieldAccessor<
      READ_ONLY,
      proxy_configuration_t,
      1,
      coord_t,
      L::AffineAccessor<proxy_configuration_t, 1, coord_t>> const acc(pr, 0);
    Configuration result;
    result.deserialize(acc.ptr(0));
    rt->unmap_region(ctx, pr);
    return result;
  }

  void
  destroy(L::Context ctx, L::Runtime* rt) {
    rt->destroy_field_space(ctx, m_lr.get_field_space());
    rt->destroy_index_space(ctx, m_lr.get_index_space());
    rt->destroy_logical_region(ctx, m_lr);
  }

  static void
  task(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& regions,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) {

    assert(regions.size() == 1);
    L::FieldAccessor<
      LEGION_WRITE_DISCARD,
      proxy_configuration_t,
      1,
      coord_t,
      L::AffineAccessor<proxy_configuration_t, 1, coord_t>> const
      acc(regions[0], 0);
    auto const& input_args = L::Runtime::get_input_args();
    // first read the configuration
    Configuration config(input_args.argc, input_args.argv);
    // now read the initial events file if needed
    if (!config.initial_events_file.empty()) {
      try {
        auto evscript = std::ifstream(
          config.initial_events_file, std::ios::binary | std::ios::ate);
        auto size = evscript.tellg();
        config.initial_events_script.resize(size, '\0');
        evscript.seekg(0);
        evscript.read(config.initial_events_script.data(), size);
      } catch (std::exception const& err) {
        config.initial_events_script.clear();
        log().warn(
          "Failed to open initial events script file '"
          + config.initial_events_file + "': " + err.what());
      }
    }
    assert(config.serialized_size() <= sizeof(proxy_configuration_t));
    config.serialize(acc.ptr(0));
  }

  static void
  preregister() {
    L::TaskVariantRegistrar registrar(task_id, task_name);
    registrar.add_constraint(L::ProcessorConstraint(L::Processor::LOC_PROC));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<task>(registrar, task_name);
  }
};
} // end namespace rcp::rcpl

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
