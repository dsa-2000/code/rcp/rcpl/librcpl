// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcpl.hpp"

#ifdef RCPL_USE_KOKKOS
#include <Kokkos_Core.hpp>
#define RCPL_USE_KOKKOS_SERIAL
#ifdef RCPL_USE_OPENMP
#define RCPL_USE_KOKKOS_OPENMP
#else // !RCPL_USE_OPENMP
#undef RCPL_USE_KOKKOS_OPENMP
#endif // !RCPL_USE_OPENMP
#ifdef RCPL_USE_CUDA
#define RCPL_USE_KOKKOS_CUDA
#else // !RCPL_USE_CUDA
#undef RCPL_USE_KOKKOS_CUDA
#endif // !RCPL_USE_CUDA
#define RCPLK_FUNCTION KOKKOS_FUNCTION
#define RCPLK_INLINE_FUNCTION KOKKOS_INLINE_FUNCTION
#define RCPLK_FORCEINLINE_FUNCTION KOKKOS_FORCEINLINE_FUNCTION
#endif // RCPL_USE_KOKKOS

namespace rcp::rcpl::k {

#ifdef RCPL_USE_KOKKOS
namespace K = Kokkos;
namespace KX = Kokkos::Experimental; // NOLINT(misc-unused-alias-decls)
#endif

#ifdef RCPL_USE_KOKKOS
constexpr bool use_kokkos = true;
#else  // !RCPL_USE_KOKKOS
constexpr bool use_kokkos = false;
#endif // RCPL_USE_KOKKOS

#ifdef RCPL_USE_KOKKOS
/*! Kokkos MDRangePolicy alias with default index type */
template <typename... Properties>
using MDRangePolicy = K::MDRangePolicy<K::IndexType<coord_t>, Properties...>;

/*! Kokkos RangePolicy alias with default index type */
template <typename... Properties>
using RangePolicy = K::RangePolicy<K::IndexType<coord_t>, Properties...>;

/*! Kokkos TeamPolicy alias with default index type */
template <typename... Properties>
using TeamPolicy = K::TeamPolicy<K::IndexType<coord_t>, Properties...>;

/*! for pre-Kokkos 4.0 */
template <typename Device>
struct SharedSpace {
  using type = K::HostSpace;
};
#ifdef RCPL_USE_KOKKOS_CUDA
template <>
struct SharedSpace<K::Cuda> {
  using type = K::CudaUVMSpace;
};
#endif // RCPL_USE_KOKKOS_CUDA
template <typename Device>
using SharedSpace_v = typename SharedSpace<Device>::type;

template <typename execution_space, typename T, typename T::Axes... axes>
auto
mdrange_policy(
  execution_space work_space, L::Rect<T::dim, coord_t> const& bounds) {

  auto constexpr dim = sizeof...(axes);
  auto lo = [&](auto&& ax) constexpr { return bounds.lo[ax]; };
  auto hi = [&](auto&& ax) constexpr { return bounds.hi[ax] + 1; };
  return MDRangePolicy<execution_space, K::Rank<dim>>(
    work_space, {lo(axes)...}, {hi(axes)...});
}

#endif // RCPL_USE_KOKKOS

/*! conversion from frequency (Hz) to wavelength (m) */
RCPL_INLINE_FUNCTION constexpr auto
wavelength(auto const& frequency) -> std::remove_cvref_t<decltype(frequency)> {
  return rcp::constants::c / frequency;
}

/*! create conversion function from meters to wave cycles at a given frequency
 *
 * \return conversion function
 */
RCPL_INLINE_FUNCTION auto
meters_to_cycles(auto const& freq) {
  return [=](auto&& meters) { return meters / wavelength(freq); };
}

/*! create conversion function from units of wave cycles at a given frequency to
 *  meters
 *
 * \return conversion function
 */
RCPL_INLINE_FUNCTION auto
cycles_to_meters(auto const& freq) {
  return [=](auto&& cycles) { return cycles * wavelength(freq); };
}

/*! convert a baseline index to a receiver index pair */
RCPL_INLINE_FUNCTION auto
baseline_to_receiver_pair(coord_t baseline) -> std::array<coord_t, 2> {
  // NOLINTBEGIN(readability-identifier-length,*-magic-numbers)
#ifdef RCPL_USE_KOKKOS
  auto p = (K::sqrt(9 + 8 * baseline) - 1) / 2;
  auto fp = K::floor(p);
  coord_t const i = std::lrint(fp);
  if (K::abs(p - fp) <= p * KX::epsilon_v<decltype(p)>)
    return {i - 1, i - 1};
  return {i, baseline - (i * (i + 1)) / 2};
#else
  auto p = (std::sqrt(9 + 8 * baseline) - 1) / 2;
  auto fp = std::floor(p);
  coord_t const i = std::lrint(fp);
  if (std::abs(p - fp) <= p * std::numeric_limits<decltype(p)>::epsilon())
    return {i - 1, i - 1};
  return {i, baseline - (i * (i + 1)) / 2};
#endif
  // NOLINTEND(readability-identifier-length,*-magic-numbers)
}

#ifdef RCPL_USE_KOKKOS
template <typename execution_space>
struct KokkosProcessor {
  static constexpr L::Processor::Kind kind = L::Processor::NO_KIND;
};
#ifdef RCPL_USE_KOKKOS_SERIAL
template <>
struct KokkosProcessor<K::Serial> {
  static constexpr L::Processor::Kind kind = L::Processor::LOC_PROC;
};
#endif // RCPL_USE_KOKKOS_SERIAL
#ifdef RCPL_USE_KOKKOS_OPENMP
template <>
struct KokkosProcessor<K::OpenMP> {
  static constexpr L::Processor::Kind kind = L::Processor::OMP_PROC;
};
#endif // RCPL_USE_KOKKOS_OPENMP
#ifdef RCPL_USE_KOKKOS_CUDA
template <>
struct KokkosProcessor<K::Cuda> {
  static constexpr L::Processor::Kind kind = L::Processor::TOC_PROC;
};
#endif // RCPL_USE_KOKKOS_CUDA

/** scoped Kokkos profiling region value */
struct ProfileRegion {
  inline ProfileRegion(const char* nm) { K::Profiling::pushRegion(nm); }

  ProfileRegion() = delete;
  ProfileRegion(ProfileRegion const&) = delete;
  ProfileRegion(ProfileRegion&&) = delete;
  auto
  operator=(ProfileRegion const&) -> ProfileRegion& = delete;
  auto
  operator=(ProfileRegion&&) -> ProfileRegion& = delete;

  inline ~ProfileRegion() { K::Profiling::popRegion(); }
};
#endif // RCPL_USE_KOKKOS

} // namespace rcp::rcpl::k

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
