// Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp/rcpl/config.hpp"
#include "rcp/rcpl/export.hpp"

#include <bark/log.hpp>
#include <legion/legion_types.h>
#include <rcp/rcp.hpp>

#include <array>
#include <limits>
#include <ranges>
#include <sstream>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>

#include <legion.h>

#if defined(RCPL_USE_CUDA) && !defined(RCPL_USE_KOKKOS)
#define RCPL_FUNCTION __host__ __device__
#else
#define RCPL_FUNCTION
#endif // RCPL_USE_CUDA

#ifndef RCPL_USE_KOKKOS
#define RCPL_INLINE_FUNCTION inline RCPL_FUNCTION
#define RCPL_FORCEINLINE_FUNCTION RCPL_INLINE_FUNCTION // TODO: OK?
#else                                                  // RCPL_USE_KOKKOS
#include <Kokkos_Core.hpp>
#include <Kokkos_OffsetView.hpp>
#define RCPL_USE_KOKKOS_SERIAL
#ifdef RCPL_USE_OPENMP
#define RCPL_USE_KOKKOS_OPENMP
#else // !RCPL_USE_OPENMP
#undef RCPL_USE_KOKKOS_OPENMP
#endif // !RCPL_USE_OPENMP
#ifdef RCPL_USE_CUDA
#define RCPL_USE_KOKKOS_CUDA
#else // !RCPL_USE_CUDA
#undef RCPL_USE_KOKKOS_CUDA
#endif // !RCPL_USE_CUDA
#define RCPL_FUNCTION KOKKOS_FUNCTION
#define RCPL_INLINE_FUNCTION KOKKOS_INLINE_FUNCTION
#define RCPL_FORCEINLINE_FUNCTION KOKKOS_FORCEINLINE_FUNCTION
#endif // RCPL_USE_KOKKOS

namespace rcp::rcpl {

struct Tag {};

} // namespace rcp::rcpl

namespace bark {

/*! static context for bark logging in rc namespace */
template <>
struct StaticContext<rcp::rcpl::Tag> {
  static constexpr char const* nm = "rcpl";
  // to avoid proliferation of configuration macros, we reuse the librcp
  // logging values
  static constexpr bark::Level min_log_level = RCP_MIN_LOG_LEVEL;
  static constexpr bool trace_location = RCP_TRACE_LOCATION;
  static constexpr bool debug_location = RCP_DEBUG_LOCATION;
  static constexpr bool info_location = RCP_INFO_LOCATION;
  static constexpr bool warn_location = RCP_WARN_LOCATION;
  static constexpr bool error_location = RCP_ERROR_LOCATION;
  static constexpr bool fatal_location = RCP_FATAL_LOCATION;
  static constexpr bool print_location = false;
  static constexpr auto
  source_identity() -> char const* {
    return nm;
  }
};

} // end namespace bark

namespace rcp::rcpl {

namespace L = Legion;

using coord_t = L::coord_t;

/*! \brief helper class for use in std::visit() */
template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

/*! create a sliced partition of an index space
 *
 * This is similar to L::Runtime::create_partition_by_blocking(), except that
 * the dimension of the color space is solely determined by the number of
 * dimensions of the index space that are sliced. Instead of returning an
 * N-dimensional partition of an N-dimensional index space, the partition can
 * have any dimension from 1 to N.
 */
template <int DIM, int COLOR_DIM, typename COORD_T>
auto
create_partition_by_slicing(
  L::Context ctx,
  L::Runtime* rt,
  L::IndexSpaceT<DIM, COORD_T> parent,
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,modernize-avoid-c-arrays)
  std::pair<int, COORD_T> const (&blocking_factor)[COLOR_DIM],
  L::Color color = LEGION_AUTO_GENERATE_ID,
  const char* provenance = nullptr) -> L::IndexPartitionT<DIM, COORD_T> {

  auto find_bf = [&](auto&& ax) {
    return std::ranges::find_if(
      blocking_factor, [&](auto&& bf) { return bf.first == ax; });
  };
  L::Transform<DIM, COLOR_DIM, COORD_T> transform;
  L::Rect<DIM, COORD_T> extent = rt->get_index_space_domain(parent).bounds;
  L::Rect<COLOR_DIM, COORD_T> colors;
  colors.lo = L::Point<COLOR_DIM, COORD_T>::ZEROES();
  for (int i = 0; i < DIM; ++i) {
    transform[i] = L::Point<COLOR_DIM, COORD_T>::ZEROES();
    auto bf = find_bf(i);
    if (bf != &blocking_factor[COLOR_DIM]) {
      auto bf_idx = std::distance(&blocking_factor[0], bf);
      auto& blk_sz = bf->second;
      transform[i][bf_idx] = blk_sz;
      colors.hi[bf_idx] =
        rcp::ceil_div(
          std::make_unsigned_t<COORD_T>(extent.hi[i] - extent.lo[i]) + 1,
          std::make_unsigned_t<COORD_T>(blk_sz))
        - 1;
      extent.hi[i] = extent.lo[i] + blk_sz - 1;
    }
  }
  return rt->create_partition_by_restriction(
    ctx,
    parent,
    rt->create_index_space(ctx, colors),
    transform,
    extent,
    LEGION_DISJOINT_COMPLETE_KIND,
    color,
    provenance);
}

/*! bark::LogSink class that integrates with Legion logging */
template <typename T>
class LogSink : public rcp::LogSink<T> {

  std::string m_instance_name{};

  L::Logger m_logger;

public:
  LogSink() noexcept : m_logger(bark::StaticContext<T>::nm) {
    this->min_level() = bark::Level::Trace; // Legion logger gets to decide
  }
  LogSink(std::string_view const& instance_name)
    : m_instance_name(instance_name), m_logger(bark::StaticContext<T>::nm) {
    this->min_level() = bark::Level::Trace; // Legion logger gets to decide
  }
  LogSink(LogSink const&) = delete;
  LogSink(LogSink&& sink) = default;
  auto
  operator=(LogSink const&) -> LogSink& = delete;
  auto
  operator=(LogSink&&) -> LogSink& = default;
  ~LogSink() = default;

  [[nodiscard]] auto
  context() const -> nlohmann::json override {
    if (!m_instance_name.empty())
      return {{"instance", m_instance_name}};
    return {};
  }

  void
  do_post(nlohmann::json const& js) override {
    std::ostringstream msg;
    std::string sep{" "};
    if (js.contains(bark::message_field_name)) {
      msg << sep << js[bark::message_field_name].get<std::string>();
      sep = "; ";
    }
    nlohmann::json stripped_js;
    for (auto&& [key, value] : js.items())
      if (
        key != bark::level_field_name && key != bark::message_field_name
        && key != bark::library_field_name) {
        stripped_js.push_back({key, value});
      }
    if (!stripped_js.is_null())
      msg << sep << stripped_js;

    switch (bark::from_tag(js[bark::level_field_name].get<std::string>())
              .value_or(bark::Level::Warn)) {
      // NOLINTBEGIN(cppcoreguidelines-pro-type-vararg)
    case bark::Level::Trace:
      m_logger.spew("%s", msg.str().c_str());
      break;
    case bark::Level::Debug:
      m_logger.debug("%s", msg.str().c_str());
      break;
    case bark::Level::Info:
      m_logger.info("%s", msg.str().c_str());
      break;
    case bark::Level::Print:
      m_logger.print("%s", msg.str().c_str());
      break;
    case bark::Level::Warn:
      m_logger.warning("%s", msg.str().c_str());
      break;
    case bark::Level::Error:
    case bark::Level::Fatal:
      m_logger.error("%s", msg.str().c_str());
      break;
      // NOLINTEND(cppcoreguidelines-pro-type-vararg)
    }
  }

  void
  flush() override {}
};

// NOLINTBEGIN(cppcoreguidelines-avoid-non-const-global-variables)
extern LogSink<rcp::Tag> log_rcp;
extern LogSink<Tag> log_rcpl;
// NOLINTEND(cppcoreguidelines-avoid-non-const-global-variables)

/*! get log sink in rcp::rcpl namespace
 *
 * In rcp::rcpl namespace we override rcp::get_log_sink() in order to get the
 * LogSink instances that use the Legion logging facility.
 */
template <typename T>
auto
get_log_sink() -> LogSink<T>&;

template <>
auto
get_log_sink<rcp::Tag>() -> LogSink<rcp::Tag>&;

template <>
auto
get_log_sink<Tag>() -> LogSink<Tag>&;

/*!  log instance for bark logging in rcpl namespace */
template <typename T = Tag>
inline auto
log() {
  return bark::Log<T>{get_log_sink<T>()};
}

/*! scope trace log instance for bark logging in rcpl namespace */
template <typename T = Tag>
inline auto
trace(std::source_location loc = bark::source_location::current()) {
  return bark::ScopeTraceLog<T>(get_log_sink<T>(), loc);
}

#ifdef RCPL_USE_KOKKOS
namespace K = Kokkos;
namespace KX = Kokkos::Experimental; // NOLINT(misc-unused-alias-decls)
#endif

#ifdef RCPL_USE_KOKKOS
constexpr bool use_kokkos = true;
#else  // !RCPL_USE_KOKKOS
constexpr bool use_kokkos = false;
#endif // RCPL_USE_KOKKOS

#ifdef RCPL_USE_KOKKOS
/*! Kokkos MDRangePolicy alias with default index type */
template <typename... Properties>
using MDRangePolicy = K::MDRangePolicy<K::IndexType<coord_t>, Properties...>;

/*! Kokkos RangePolicy alias with default index type */
template <typename... Properties>
using RangePolicy = K::RangePolicy<K::IndexType<coord_t>, Properties...>;

/*! Kokkos TeamPolicy alias with default index type */
template <typename... Properties>
using TeamPolicy = K::TeamPolicy<K::IndexType<coord_t>, Properties...>;

/*! for pre-Kokkos 4.0 */
template <typename Device>
struct SharedSpace {
  using type = K::HostSpace;
};
#ifdef RCPL_USE_KOKKOS_CUDA
template <>
struct SharedSpace<K::Cuda> {
  using type = K::CudaUVMSpace;
};
#endif // RCPL_USE_KOKKOS_CUDA
template <typename Device>
using SharedSpace_v = typename SharedSpace<Device>::type;

template <typename execution_space, typename T, typename T::Axes... axes>
auto
mdrange_policy(
  execution_space work_space, L::Rect<T::dim, coord_t> const& bounds) {

  auto constexpr dim = sizeof...(axes);
  auto lo = [&](auto&& ax) constexpr { return bounds.lo[ax]; };
  auto hi = [&](auto&& ax) constexpr { return bounds.hi[ax] + 1; };
  return MDRangePolicy<execution_space, K::Rank<dim>>(
    work_space, {lo(axes)...}, {hi(axes)...});
}

template <typename execution_space, typename T, typename T::axes_t... axes>
auto
mdrange_policy(execution_space work_space, typename T::rect_t const& bounds) {

  auto constexpr dim = sizeof...(axes);
  auto lo = [&](auto&& ax) constexpr {
    return bounds.lo[static_cast<int>(ax)];
  };
  auto hi = [&](auto&& ax) constexpr {
    return bounds.hi[static_cast<int>(ax)] + 1;
  };
  return MDRangePolicy<execution_space, K::Rank<dim>>(
    work_space, {lo(axes)...}, {hi(axes)...});
}

#endif // RCPL_USE_KOKKOS

/*! conversion from frequency (Hz) to wavelength (m) */
RCPL_INLINE_FUNCTION constexpr auto
wavelength(auto const& frequency) -> std::remove_cvref_t<decltype(frequency)> {
  return rcp::constants::c / frequency;
}

/*! create conversion function from meters to wave cycles at a given frequency
 *
 * \return conversion function
 */
RCPL_INLINE_FUNCTION auto
meters_to_cycles(auto const& freq) {
  return [=](auto&& meters) { return meters / wavelength(freq); };
}

/*! create conversion function from units of wave cycles at a given frequency to
 *  meters
 *
 * \return conversion function
 */
RCPL_INLINE_FUNCTION auto
cycles_to_meters(auto const& freq) {
  return [=](auto&& cycles) { return cycles * wavelength(freq); };
}

/*! convert a baseline index to a receiver index pair */
RCPL_INLINE_FUNCTION auto
baseline_to_receiver_pair(coord_t baseline) -> std::array<coord_t, 2> {
  // NOLINTBEGIN(readability-identifier-length,*-magic-numbers)
#ifdef RCPL_USE_KOKKOS
  auto p = (K::sqrt(9 + 8 * baseline) - 1) / 2;
  auto fp = K::floor(p);
  coord_t const i = std::lrint(fp);
  if (K::abs(p - fp) <= p * KX::epsilon_v<decltype(p)>)
    return {i - 1, i - 1};
  return {i, baseline - (i * (i + 1)) / 2};
#else
  auto p = (std::sqrt(9 + 8 * baseline) - 1) / 2;
  auto fp = std::floor(p);
  coord_t const i = std::lrint(fp);
  if (std::abs(p - fp) <= p * std::numeric_limits<decltype(p)>::epsilon())
    return {i - 1, i - 1};
  return {i, baseline - (i * (i + 1)) / 2};
#endif
  // NOLINTEND(readability-identifier-length,*-magic-numbers)
}

#ifdef RCPL_USE_KOKKOS
template <typename execution_space>
struct KokkosProcessor {
  static constexpr L::Processor::Kind kind = L::Processor::NO_KIND;
};
#ifdef RCPL_USE_KOKKOS_SERIAL
template <>
struct KokkosProcessor<K::Serial> {
  static constexpr L::Processor::Kind kind = L::Processor::LOC_PROC;
};
#endif // RCPL_USE_KOKKOS_SERIAL
#ifdef RCPL_USE_KOKKOS_OPENMP
template <>
struct KokkosProcessor<K::OpenMP> {
  static constexpr L::Processor::Kind kind = L::Processor::OMP_PROC;
};
#endif // RCPL_USE_KOKKOS_OPENMP
#ifdef RCPL_USE_KOKKOS_CUDA
template <>
struct KokkosProcessor<K::Cuda> {
  static constexpr L::Processor::Kind kind = L::Processor::TOC_PROC;
};
#endif // RCPL_USE_KOKKOS_CUDA

/** scoped Kokkos profiling region value */
struct ProfileRegion {
  inline ProfileRegion(const char* nm) { K::Profiling::pushRegion(nm); }

  ProfileRegion() = delete;
  ProfileRegion(ProfileRegion const&) = delete;
  ProfileRegion(ProfileRegion&&) = delete;
  auto
  operator=(ProfileRegion const&) -> ProfileRegion& = delete;
  auto
  operator=(ProfileRegion&&) -> ProfileRegion& = delete;

  inline ~ProfileRegion() { K::Profiling::popRegion(); }
};
#endif // RCPL_USE_KOKKOS

#ifdef RCPL_USE_KOKKOS

/* \brief pointer type for values in a Kokkos View of some dimension
 *
 * \tparam T value type
 * \tparam Dim View dimension
 */
template <typename T, auto Dim>
struct ptype {
  using type = ptype<T, Dim - 1>::type*;
};
template <typename T>
struct ptype<T, 0> {
  using type = T;
};
/*! \brief convenience type for access to ptime<>::type */
template <typename T, auto Dim>
using ptype_t = ptype<T, Dim>::type;

/*! \brief static region field representation
 *
 * \tparam T value type
 * \tparam FieldID field id
 * \tparam SerdezID pointer to serdez id
 * \tparam KT value type for Kokkos views
 * \tparam RT value type for Legion reductions
 */
template <
  typename T,
  L::FieldID FieldID,
  L::CustomSerdezID* SerdezID = nullptr,
  typename KT = T,
  typename RT = T>
struct StaticField {
  /* \brief cv-unqualified value type */
  using value_t = std::remove_cvref_t<T>;
  /* \brief value type for Kokkos views */
  using kvalue_t = std::remove_cvref_t<KT>;
  /*! \brief value type for Legion reductions */
  using rvalue_t = std::remove_cvref_t<RT>;
  /* \brief field id */
  static constexpr auto field_id = FieldID;
  /*! \brief serdez id */
  static constexpr auto serdez_id = SerdezID;
};

template <typename T>
concept IsStaticField = requires {
  typename T::value_t;
  typename T::kvalue_t;
  typename T::rvalue_t;
  { T::field_id } -> std::convertible_to<L::FieldID>;
  { T::serdez_id } -> std::convertible_to<L::CustomSerdezID*>;
};
#else
/*! \brief static region field representation
 *
 * \tparam T value type
 * \tparam FieldID field id
 * \tparam SerdezID pointer to serdez id
 */
template <
  typename T,
  L::FieldID FieldID,
  L::CustomSerdezID* SerdezID = nullptr,
  typename RT = T>
struct StaticField {
  /* \brief cv-unqualified value type */
  using value_t = std::remove_cvref_t<T>;
  /*! \brief value type for Legion reductions */
  using rvalue_t = std::remove_cvref_t<RT>;
  /* \brief field id */
  static constexpr auto field_id = FieldID;
  /*! \brief serdez id */
  static constexpr auto serdez_id = SerdezID;
};

template <typename T>
concept IsStaticField = requires {
  typename T::value_t;
  typename T::rvalue_t;
  { T::field_id } -> std::convertible_to<L::FieldID>;
  { T::serdez_id } -> std::convertible_to<L::CustomSerdezID*>;
};
#endif // RCPL_USE_KOKKOS

template <IsStaticField... Fields>
struct StaticFields {
  constexpr StaticFields(Fields&&... /*fields*/){};
  static constexpr auto
  apply(auto&& fn) -> void {
    (fn(Fields{}), ...);
  }
};

/*! \brief interval of index values
 *
 * \tparam Coord coordinate (index) type
 */
template <typename Coord>
struct IndexInterval {
  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  bool m_has_left{false};  /*!< whether left bound is explicit */
  bool m_has_right{false}; /*!< whether right bound is explicit */
  Coord m_left{};          /*!< left bound (inclusive) */
  Coord m_right{};         /*!< right bound (inclusive) */
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  /*! \brief default constructor (defaulted)
   *
   * Results in unbounded interval.
   */
  constexpr IndexInterval() noexcept = default;
  // NOLINTBEGIN(bugprone-easily-swappable-parameters)
  /*! \brief bounded interval constructor
   *
   * \param _left left bound (inclusive)
   * \param _right right bound (inclusive)
   */
  constexpr IndexInterval(Coord _left, Coord _right) noexcept
    : m_has_left(true), m_has_right(true), m_left(_left), m_right(_right) {
    assert(left() <= right());
  }
  /*! \brief general interval constructor
   *
   * \param _left optional left bound (inclusive)
   * \param _right optional right bound (inclusive)
   *
   * std::nullopt values are used to indicate that the bound has no explicit
   * value (although the limits of the Coord type still exist)
   */
  constexpr IndexInterval(
    std::optional<Coord> _left, std::optional<Coord> _right) noexcept
    : m_has_left(_left.has_value())
    , m_has_right(_right.has_value())
    , m_left(_left.value_or(Coord{}))
    , m_right(_right.value_or(Coord{})) {
    assert(left() <= right());
  }
  // NOLINTEND(bugprone-easily-swappable-parameters)
  /*! \brief copy constructor (defaulted) */
  IndexInterval(IndexInterval const&) noexcept = default;
  /*! \brief move constructor (defaulted) */
  IndexInterval(IndexInterval&&) noexcept = default;
  /*! \brief destructor (defaulted) */
  ~IndexInterval() = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(IndexInterval const&) noexcept -> IndexInterval& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(IndexInterval&&) noexcept -> IndexInterval& = default;

  /*! \brief equality operator
   *
   * Unbounded endpoints (i.e, those not explicitly given a value by the
   * constructor) are considered as distinct values, even when the effective
   * values are the numeric limits of the Coord type.
   */
  auto
  operator==(IndexInterval const& rhs) const noexcept -> bool {
    return ((m_has_left == rhs.m_has_left)
            && (!m_has_left || (m_left == rhs.m_left)))
           && ((m_has_right == rhs.m_has_right) && (!m_has_right || (m_right == rhs.m_right)));
  }

  /*! \brief left endpoint
   *
   * \return left interval endpoint, or minimum value of the Coord type if not
   * explictly set
   */
  [[nodiscard]] constexpr auto
  left() const noexcept -> Coord {
    return m_has_left ? m_left : std::numeric_limits<Coord>::min();
  }

  /*! \brief right endpoint
   *
   * \return right interval endpoint, or maximum value of the Coord type if not
   * explictly set
   */
  [[nodiscard]] constexpr auto
  right() const noexcept -> Coord {
    return m_has_right ? m_right : std::numeric_limits<Coord>::max();
  }

  /*! \brief test for unbounded interval */
  [[nodiscard]] constexpr auto
  is_unbounded() const noexcept -> bool {
    return !m_has_left && !m_has_right;
  }
  /*! \brief test for interval bounded only on the left */
  [[nodiscard]] constexpr auto
  is_left_bounded() const noexcept -> bool {
    return m_has_left && !m_has_right;
  }
  /*! \brief test for interval bounded only on the right */
  [[nodiscard]] constexpr auto
  is_right_bounded() const noexcept -> bool {
    return !m_has_left && m_has_right;
  }
  /*! \brief test for bounded interval */
  [[nodiscard]] constexpr auto
  is_bounded() const noexcept -> bool {
    return m_has_left && m_has_right;
  }

  /*! \brief size of interval (inclusive) */
  [[nodiscard]] constexpr auto
  size() const noexcept -> Coord {
    return right() - left() + 1;
  }

  /*! \brief intersection with another interval
   *
   * \param rhs another IndexInterval
   *
   * \result intersection interval
   */
  [[nodiscard]] auto
  intersection(IndexInterval const& rhs) const -> std::optional<IndexInterval> {
    auto lft = std::max(left(), rhs.left());
    auto rgt = std::min(right(), rhs.right());
    if (lft <= rgt)
      return std::make_optional<IndexInterval>(
        (m_has_left || rhs.m_has_left) ? std::make_optional(lft) : std::nullopt,
        (m_has_right || rhs.m_has_right) ? std::make_optional(rgt)
                                         : std::nullopt);
    return std::nullopt;
  }

  static constexpr auto
  unbounded() -> IndexInterval {
    return IndexInterval{};
  }

  static constexpr auto
  left_bounded(Coord left) -> IndexInterval {
    return IndexInterval{left, std::nullopt};
  }

  static constexpr auto
  right_bounded(Coord right) -> IndexInterval {
    return IndexInterval{std::nullopt, right};
  }

  static constexpr auto
  bounded(Coord left, Coord right) -> IndexInterval {
    return IndexInterval{left, right};
  }
};

/*! \brief concept for an "index specification"
 *
 * This is specification of a set of axes, the type of index values, and
 * associated Legion datatypes derived from those definitions.
 */
template <typename T>
concept IsIndexSpec = requires {
  typename T::coord_t;
  typename T::axes_t;
  typename T::index_space_t;
  typename T::index_partition_t;
  typename T::logical_region_t;
  typename T::logical_partition_t;
  typename T::domain_t;
  typename T::rect_t;
  typename T::range_t;
  { T::dim } -> std::convertible_to<int>;
};

/*! \brief generic type for IsIndexSpec values
 *
 * Specializations of this type may be used to create classes that satisfy the
 * IsIndexSpec concept; however, it is not a requirement.
 *
 * \tparam Axes scoped enumeration defining the index space axes (dimensions)
 * \tparam first first value of the Axes enumeration
 * \tparam last last value of the Axes enumeration
 * \tparam Coord index coordinate value type
 *
 * It is required that first value of Axes converted to an integer is equal to
 * LEGION_DIM_0, and that the set if values of Axes converted to integers is
 * consecutive. The first requirement is checked statically, but the second
 * requirement is not.
 */
template <typename Axes, Axes first, Axes last, typename Coord>
struct IndexSpec {

  static_assert(
    static_cast<std::underlying_type_t<Axes>>(first) == LEGION_DIM_0);

  /*! \brief space dimension */
  static constexpr auto dim =
    static_cast<std::underlying_type_t<Axes>>(last) + 1;
  /*! \brief Axes type alias */
  using axes_t = Axes;
  /*! \brief Coord type alias */
  using coord_t = Coord;
  /*! \brief L::IndexSpaceT type alias */
  using index_space_t = L::IndexSpaceT<dim, coord_t>;
  /*! \brief L::IndexPartitionT type alias */
  using index_partition_t = L::IndexPartitionT<dim, coord_t>;
  /*! \brief L::LogicalRegionT type alias */
  using logical_region_t = L::LogicalRegionT<dim, coord_t>;
  /*! \brief L::LogicalPartitionT type alias */
  using logical_partition_t = L::LogicalPartitionT<dim, coord_t>;
  /*! \brief L::DomainT type alias */
  using domain_t = L::DomainT<dim, coord_t>;
  /*! \brief L::Rect type alias */
  using rect_t = L::Rect<dim, coord_t>;
  /*! \brief type alias for index range in one dimension */
  using range_t = IndexInterval<coord_t>;
};

/*! \brief field accessor
 *
 * Stretches the definition of "accessor", because in addition to a region
 * accessor creation function, this provides a function to add a field to a
 * RegionRequirement. The added value comes from the type tags used by the
 * functions, which support access to fields by their types.
 *
 * \tparam IndexSpec specification of an IndexSpace (used for coordinate type
 * and dimension value)
 * \tparam Field StaticField type
 */
template <IsIndexSpec IndexSpec, typename Field>
struct StaticFieldAccessor {

  /*! \brief add this field to a RegionRequirement
   *
   * \param req RegionRequirement
   * \param field field value (for typed function overloading, value not used)
   *
   * \return the RegionRequirement value after the field has been added
   */
  static auto
  add_field(L::RegionRequirement& req, Field const& /*field*/)
    -> L::RegionRequirement& {
    return req.add_field(Field::field_id);
  }

  /*! \brief convert Field type to FieldID
   *
   * Used for matching on Field type and converting it to a FieldID value
   */
  static auto constexpr field_id(Field const& /*field*/) -> L::FieldID {
    return Field::field_id;
  }

  /*! \brief type alias for accessors to field in a PhysicalRegion
   *
   * \tparam Mode access privilege
   * \tparam Affine flag for affine index space
   * \tparam T value type
   */
  template <L::PrivilegeMode Mode, bool Affine, typename T>
  using accessor_t = std::conditional_t<
    Affine,
    L::FieldAccessor<
      Mode,
      T,
      IndexSpec::dim,
      typename IndexSpec::coord_t,
      L::AffineAccessor<T, IndexSpec::dim, typename IndexSpec::coord_t>>,
    L::FieldAccessor<Mode, T, IndexSpec::dim, typename IndexSpec::coord_t>>;

  /*! \brief type alias for reduction accessors to field in a PhysicalRegion
   *
   * \tparam R reduction op
   * \tparam Exclusive flag for exclusive reduction access
   * \tparam Affine flag for affine index space
   * \tparam T value type
   */
  template <typename R, bool Exclusive, bool Affine, typename T>
  using reduction_accessor_t = std::conditional_t<
    Affine,
    L::ReductionAccessor<
      R,
      Exclusive,
      IndexSpec::dim,
      typename IndexSpec::coord_t,
      L::AffineAccessor<T, IndexSpec::dim, typename IndexSpec::coord_t>>,
    L::ReductionAccessor<
      R,
      Exclusive,
      IndexSpec::dim,
      typename IndexSpec::coord_t>>;

  /*! \brief construct accessor to field in a PhysicalRegion instance
   *
   * \tparam Mode access privilege
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   */
  template <
    L::PrivilegeMode Mode,
    bool Affine = true,
    typename T = typename Field::value_t>
  static auto
  values(L::PhysicalRegion const& pr, Field const& /*field*/) {
    return accessor_t<Mode, Affine, T>(pr, Field::field_id);
  }

  /*! \brief construct accessor to field in sub-region of a PhysicalRegion
   *  instance
   *
   * \tparam Mode access privilege
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   * \param bounds sub-region bounds
   */
  template <
    L::PrivilegeMode Mode,
    bool Affine = true,
    typename T = typename Field::value_t>
  static auto
  values(
    L::PhysicalRegion const& pr,
    Field const& /*field*/,
    IndexSpec::rect_t const& bounds) {
    return accessor_t<Mode, Affine, T>(pr, Field::field_id, bounds);
  }

  /*! \brief construct reduction accessor to field in a PhysicalRegion instance
   *
   * \tparam R reduction op
   * \tparam Exclusive flag for exclusive reduction access
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   */
  template <
    typename R,
    bool Exclusive,
    bool Affine = true,
    typename T = typename Field::rvalue_t>
  static auto
  values(L::PhysicalRegion const& pr, Field const& /*field*/) {
    return reduction_accessor_t<R, Exclusive, Affine, T>(
      pr, Field::field_id, R::REDOP_ID);
  }

  /*! \brief construct reduction accessor to field in sub-region of a
   *  PhysicalRegion instance
   *
   * \tparam R reduction op
   * \tparam Exclusive flag for exclusive reduction access
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   * \param bounds sub-region bounds
   */
  template <
    typename R,
    bool Exclusive,
    bool Affine = true,
    typename T = typename Field::rvalue_t>
  static auto
  values(
    L::PhysicalRegion const& pr,
    Field const& /*field*/,
    IndexSpec::rect_t const& bounds) {
    return reduction_accessor_t<R, Exclusive, Affine, T>(
      pr, Field::field_id, R::REDOP_ID, bounds);
  }

#ifdef RCPL_USE_KOKKOS
  /*! \brief Kokkos view value type */
  using kvalue_t = std::conditional_t<
    std::is_void_v<typename Field::kvalue_t>,
    typename Field::value_t,
    typename Field::kvalue_t>;

  /*! \brief Kokkos view accessor type for region
   *
   * \tparam execution_space Kokkos execution space
   * \tparam ReadOnly flag for read-only access
   */
  template <typename execution_space, bool ReadOnly, typename T>
  using view_t = Kokkos::Experimental::OffsetView<
    ptype_t<std::conditional_t<ReadOnly, T const, T>, IndexSpec::dim>,
    Kokkos::LayoutStride,
    typename execution_space::memory_space>;

  /*! \brief construct Kokkos view accessor to field in a PhysicalRegion
   * instance
   *
   * \tparam execution_space Kokkos execution space
   * \tparam Mode access privilege
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   */
  template <
    typename execution_space,
    L::PrivilegeMode Mode,
    bool Affine = true,
    typename T = typename Field::kvalue_t>
  static auto
  view(L::PhysicalRegion const& pr, Field const& /*field*/)
    -> view_t<execution_space, (Mode == LEGION_READ_ONLY), T> {
    return values<Mode, Affine, T>(pr, Field{}).accessor;
  }

  /*! \brief construct Kokkos view accessor to field in a sub-region of a
   * PhysicalRegion instance
   *
   * \tparam execution_space Kokkos execution space
   * \tparam Mode access privilege
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   * \param bounds sub-region bounds
   */
  template <
    typename execution_space,
    L::PrivilegeMode Mode,
    bool Affine = true,
    typename T = typename Field::kvalue_t>
  static auto
  view(
    L::PhysicalRegion const& pr,
    Field const& /*field*/,
    typename IndexSpec::rect_t const& bounds)
    -> view_t<execution_space, (Mode == LEGION_READ_ONLY), T> {
    return values<Mode, Affine, T>(pr, Field{}, bounds).accessor;
  }

  /*! \brief construct Kokkos view accessor to reduction field a PhysicalRegion
   *  instance
   *
   * \tparam execution_space Kokkos execution space
   * \tparam R reduction op
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   *
   * No Exclusive template parameter, because the Kokkos view is constructed
   * from the underlying Realm accessor, which knows nothing about exclusive vs
   * non-exclusive access.
   */
  template <
    typename execution_space,
    typename R,
    bool Affine = true,
    typename T = typename Field::rvalue_t>
  static auto
  view(L::PhysicalRegion const& pr, Field const& /*field*/)
    -> view_t<execution_space, false, T> {
    return values<R, true, Affine, T>(pr, Field{}).accessor;
  }

  /*! \brief construct Kokkos view accessor to reduction field in sub-region of
   * a PhysicalRegion instance
   *
   * \tparam execution_space Kokkos execution space
   * \tparam R reduction op
   * \tparam Affine flag for affine index space
   * \tparam T value type
   *
   * \param pr PhysicalRegion instance
   * \param field field value (for tagged function overloading, value not used)
   * \param bounds sub-region bounds
   *
   * No Exclusive template parameter, because the Kokkos view is constructed
   * from the underlying Realm accessor, which knows nothing about exclusive vs
   * non-exclusive access.
   */
  template <
    typename execution_space,
    typename R,
    bool Affine = true,
    typename T = typename Field::rvalue_t>
  static auto
  view(
    L::PhysicalRegion const& pr,
    Field const& /*field*/,
    typename IndexSpec::rect_t const& bounds)
    -> view_t<execution_space, false, T> {
    return values<R, true, Affine, T>(pr, Field{}, bounds).accessor;
  }
#endif
};

/* \brief mixin class that adds functions from StaticFieldAccessor for a
 * collection of fields
 *
 * \tparam IndexSpec specification of an IndexSpec
 * \tparam Fields a number of StaticField types
 */
template <IsIndexSpec IndexSpec, typename... Fields>
struct StaticFieldMixin : private StaticFieldAccessor<IndexSpec, Fields>... {
  static_assert(
    ((std::ranges::count(std::array{Fields{}.field_id...}, Fields{}.field_id)
      == 1)
     && ...));
  using StaticFieldAccessor<IndexSpec, Fields>::add_field...;
  using StaticFieldAccessor<IndexSpec, Fields>::field_id...;
  using StaticFieldAccessor<IndexSpec, Fields>::values...;
  using StaticFieldAccessor<IndexSpec, Fields>::view...;
};

/*! \brief partial statically defined region specification
 *
 * \tparam IndexSpec specification of an IndexSpace
 * \tparam StaticBounds static index space bounds
 * \tparam Fields a number of StaticField types
 * *
 */
template <
  IsIndexSpec IndexSpec,
  std::array<IndexInterval<typename IndexSpec::coord_t>, IndexSpec::dim>
    StaticBounds,
  typename... Fields>
class StaticRegionSpec
  : public IndexSpec
  , public StaticFieldMixin<IndexSpec, Fields...> {
public:
  /*! \brief type alias for static bounds values */
  using static_bounds_t =
    std::array<typename IndexSpec::range_t, IndexSpec::dim>;

  /*! \brief static bounds */
  static constexpr static_bounds_t static_bounds = StaticBounds;

  /*! \brief type alias for dynamic bounds values */
  using dynamic_bounds_t =
    std::map<typename IndexSpec::axes_t, typename IndexSpec::range_t>;

  /*! \brief wrapper for logical regions of the type defined by this
   * PartialStaticRegionSpec class
   */
  class LogicalRegion {
    /*! \brief the wrapped L::LogicalRegion instance */
    typename IndexSpec::logical_region_t m_handle{};

  public:
    using index_spec_t = IndexSpec;

    /*! \brief default constructor (defaulted) */
    explicit LogicalRegion() = default;
    /*! \brief constructor from a L::LogicalRegionT instance */
    explicit LogicalRegion(typename IndexSpec::logical_region_t handle)
      : m_handle(handle) {}
    /*! \brief constructor from a L::LogicalRegion instance */
    explicit LogicalRegion(L::LogicalRegion handle) : m_handle(handle) {}
    /*! \brief copy constructor (defaulted) */
    LogicalRegion(LogicalRegion const&) = default;
    /*! \brief move constructor (defaulted) */
    LogicalRegion(LogicalRegion&&) noexcept = default;
    /*! \brief destructor (defaulted) */
    ~LogicalRegion() = default;
    /*! \brief copy assignment operator (defaulted) */
    auto
    operator=(LogicalRegion const&) -> LogicalRegion& = default;
    /*! \brief move assignment operator (defaulted) */
    auto
    operator=(LogicalRegion&&) noexcept -> LogicalRegion& = default;

    /*! \brief test for compatibility of some L::LogicalRegion */
    static auto
    is_compatible(L::Runtime* rt, L::LogicalRegion const& region) -> bool {
      // check dimension
      if (region.get_dim() != IndexSpec::dim)
        return false;
      // check that bounds are compatible with StaticBounds
      L::Rect<IndexSpec::dim, coord_t> rect =
        rt->get_index_space_domain(region.get_index_space())
          .bounds<IndexSpec::dim>();
      for (auto&& bnd : StaticBounds) {
        auto idx = std::distance(StaticBounds.begin(), &bnd);
        if (bnd.is_bounded()) {
          if (rect.lo[idx] < bnd.left() || bnd.right() < rect.hi[idx])
            return false;
        } else if (bnd.is_left_bounded()) {
          if (rect.lo[idx] < bnd.left())
            return false;
        } else if (bnd.is_right_bounded()) {
          if (bnd.right() < rect.hi[idx])
            return false;
        }
      }
      // check that field space has static fields
      auto fs = region.get_field_space();
      std::set<L::FieldID> fields;
      rt->get_field_space_fields(fs, fields);
      return std::ranges::includes(fields, std::set{Fields{}.field_id...});
    }

    /*! \brief create a L::RegionRequirement for this LogicalRegion
     *
     * \tparam Fs list of StaticField types
     *
     * \param priv_or_redop requirement privilege mode or reduction op id
     * \param coherence requirement coherence
     * \param fs fields wrapped in StaticFields (for typed function overloading,
     * value not used)
     * \param proj optional projection id
     * \param parent optional parent region
     * \param tag mapping tag id
     *
     * \return L::RegionRequirement instance with given privilege and coherence
     * for the requested fields
     *
     * Note that the fields are wrapped by StaticFields to allow a single
     * implementation using arguments with default values.
     */
    template <IsStaticField... Fs>
    [[nodiscard]] auto
    requirement(
      std::variant<L::PrivilegeMode, L::ReductionOpID> priv_or_redop,
      L::CoherenceProperty coherence,
      StaticFields<Fs...>&& /*fields*/,
      std::optional<L::ProjectionID> const& proj = std::nullopt,
      std::optional<LogicalRegion> const& parent = std::nullopt,
      L::MappingTagID tag = 0) const -> L::RegionRequirement {

      auto result = std::visit(
        [&, this](auto&& pr) {
          return L::RegionRequirement(
            *this, pr, coherence, parent.value_or(*this), tag);
        },
        priv_or_redop);
      if (proj)
        result.projection = *proj;
      StaticFields<Fs...>::apply(
        [&, this](auto&& fld) { add_fields(result, fld); });
      return result;
    }

    /*! \brief fill fields in the region
     *
     * \tparam Fs field types
     *
     * \param ctx Legion context
     * \param rt Legion runtime
     * \param fvs field-value pairs, where the value provides the fill value of
     * the field
     */
    template <IsStaticField... Fs>
    auto
    fill_fields(
      L::Context ctx,
      L::Runtime* rt,
      std::tuple<Fs, typename Fs::value_t>&&... fvs) const -> void {
      // using the field_id() function instead of using Fs::field_id provides
      // type safety for the static fields defined by this region
      (rt->fill_field(
         ctx,
         *this,
         *this,
         StaticFieldMixin<IndexSpec, Fields...>::field_id(std::get<0>(fvs)),
         std::get<1>(fvs)),
       ...);
    }

    /*! \brief fill fields in the region
     *
     * \tparam Fs field types
     *
     * \param ctx Legion context
     * \param rt Legion runtime
     * \param parent parent region
     * \param fvs field-value pairs, where the value provides the fill value of
     * the field
     */
    template <IsStaticField... Fs>
    auto
    fill_fields(
      L::Context ctx,
      L::Runtime* rt,
      std::optional<L::LogicalRegion> const& parent,
      std::optional<L::Predicate> const& pred,
      std::tuple<Fs, typename Fs::value_t>&&... fvs) const -> void {
      // using the field_id() function instead of using Fs::field_id provides
      // type safety for the static fields defined by this region
      (rt->fill_field(
         ctx,
         *this,
         parent.value_or(*this),
         StaticFieldMixin<IndexSpec, Fields...>::field_id(std::get<0>(fvs)),
         std::get<1>(fvs),
         pred.value_or(L::Predicate::TRUE_PRED)),
       ...);
    }

    /*! \brief type conversion operator to IndexSpec::logical_region_t */
    operator typename IndexSpec::logical_region_t() const { return m_handle; };

    /*! \brief equality operator (mirrors function of L::LogicalRegion) */
    auto
    operator==(const LogicalRegion& rhs) const -> bool {
      return m_handle == rhs.m_handle;
    }

    /*! \brief inequality operator (mirrors function of L::LogicalRegion) */
    auto
    operator!=(const LogicalRegion& rhs) const -> bool {
      return m_handle != rhs.m_handle;
    }

    /*! \brief less-than operator (mirrors function of L::LogicalRegion) */
    auto
    operator<(const LogicalRegion& rhs) const -> bool {
      return m_handle < rhs.m_handle;
    }

    /*! \brief region's index space (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    get_index_space() const -> typename IndexSpec::index_space_t {
      return typename IndexSpec::index_space_t{m_handle.get_index_space()};
    }

    /*! \brief region's field space (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    get_field_space() const -> L::FieldSpace {
      return m_handle.get_field_space();
    }

    /*! \brief region's tree id (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    get_tree_id() const -> L::RegionTreeID {
      return m_handle.get_tree_id();
    }

    /*! \brief region existence test (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    exists() const -> bool {
      return m_handle.exists();
    }

    /*! \brief region's type tag (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    get_type_tag() const -> L::TypeTag {
      return m_handle.get_type_tag();
    }

    /*! \brief region's dimension (mirrors function of L::LogicalRegion) */
    [[nodiscard]] auto
    get_dim() const -> int {
      return m_handle.get_dim();
    }
  };

  /*! \brief wrapper for logical partitions of the regions defined by this
   * PartialStaticRegionSpec class
   */
  class LogicalPartition {
    /*! \brief the wrapped L::LogicalPartition instance */
    typename IndexSpec::logical_partition_t m_handle{};

  public:
    using index_spec_t = IndexSpec;
    using region_t = LogicalRegion;

    /*! \brief default constructor (defaulted) */
    explicit LogicalPartition() = default;
    /*! \brief constructor from a L::LogicalPartition instance */
    explicit LogicalPartition(typename IndexSpec::logical_partition_t handle)
      : m_handle(handle) {}
    explicit LogicalPartition(L::LogicalPartition handle) : m_handle(handle) {}
    /*! \brief copy constructor (defaulted) */
    LogicalPartition(LogicalPartition const&) = default;
    /*! \brief move constructor (defaulted) */
    LogicalPartition(LogicalPartition&&) noexcept = default;
    /*! \brief destructor (defaulted) */
    ~LogicalPartition() = default;
    /*! \brief copy assignment operator (defaulted) */
    auto
    operator=(LogicalPartition const&) -> LogicalPartition& = default;
    /*! \brief move assignment operator (defaulted) */
    auto
    operator=(LogicalPartition&&) noexcept -> LogicalPartition& = default;

    /*! \brief test for compatibility of some L::LogicalPartition */
    static auto
    is_compatible(L::Runtime* rt, L::LogicalPartition const& partition)
      -> bool {
      // check dimension
      if (partition.get_dim() != IndexSpec::dim)
        return false;
      // check that parent index space bounds are compatible with StaticBounds
      L::Rect<IndexSpec::dim, coord_t> rect =
        rt->get_index_space_domain(
            rt->get_parent_index_space(partition.get_index_partition()))
          .bounds<IndexSpec::dim>();
      for (auto&& bnd : StaticBounds) {
        auto idx = std::distance(StaticBounds.begin(), &bnd);
        if (bnd.is_bounded()) {
          if (bnd.left() != rect.lo[idx] || bnd.right() != rect.hi[idx])
            return false;
        } else if (bnd.is_left_bounded()) {
          if (bnd.left() != rect.lo[idx])
            return false;
        } else if (bnd.is_right_bounded()) {
          if (bnd.right() != rect.hi[idx])
            return false;
        }
      }
      // check that field space has static fields
      auto fs = partition.get_field_space();
      std::set<L::FieldID> fields;
      rt->get_field_space_fields(fs, fields);
      return std::ranges::includes(fields, std::set{Fields{}.field_id...});
    }

    /*! \brief create a L::RegionRequirement for this LogicalPartition
     *
     * \tparam Fs list of StaticField types
     *
     * \param priv_or_redop requirement privilege mode or reduction op id
     * \param coherence requirement coherence
     * \param rt_or_parent L::Runtime or parent region
     * \param fs fields wrapped in StaticFields (for typed function overloading,
     *        value not used)
     * \param tag mapping tag id
     * \param redop optional reduction op id
     * \param proj optional projection id
     *
     * \return L::RegionRequirement instance with given privilege and coherence
     * for the requested fields
     *
     * Note that the fields are wrapped by StaticFields to allow a single
     * implementation using arguments with default values. If L::Runtime is
     * provided by rt_or_parent, the parent region of this partition as detected
     * by the Legion runtime is used.
     */
    template <IsStaticField... Fs>
    [[nodiscard]] auto
    requirement(
      L::ProjectionID proj,
      std::variant<L::PrivilegeMode, L::ReductionOpID> priv_or_redop,
      L::CoherenceProperty coherence,
      std::variant<L::Runtime*, LogicalRegion> const& rt_or_parent,
      StaticFields<Fs...>&& /*fields*/,
      L::MappingTagID tag = 0) const -> L::RegionRequirement {

      auto parent = std::visit(
        overloaded{
          [this](L::Runtime* rt) -> typename IndexSpec::logical_region_t {
            return typename IndexSpec::logical_region_t{
              rt->get_parent_logical_region(*this)};
          },
          [](LogicalRegion const& lr) ->
          typename IndexSpec::logical_region_t { return lr; }},
        rt_or_parent);
      auto result = std::visit(
        [&, this](auto&& pr) {
          return L::RegionRequirement(*this, proj, pr, coherence, parent, tag);
        },
        priv_or_redop);
      StaticFields<Fs...>::apply(
        [&, this](auto&& fld) { add_fields(result, fld); });
      return result;
    }

    /*! \brief type conversion operator to IndexSpec::logical_partition_t */
    operator typename IndexSpec::logical_partition_t() const {
      return m_handle;
    };

    /*! \brief equality operator (mirrors function of L::LogicalPartition) */
    auto
    operator==(const LogicalPartition& rhs) const -> bool {
      return m_handle == rhs.m_handle;
    }

    /*! \brief inequality operator (mirrors function of L::LogicalPartition) */
    auto
    operator!=(const LogicalPartition& rhs) const -> bool {
      return m_handle != rhs.m_handle;
    }

    /*! \brief less-than operator (mirrors function of L::LogicalPartition) */
    auto
    operator<(const LogicalPartition& rhs) const -> bool {
      return m_handle < rhs.m_handle;
    }

    /*! \brief partition's index partition (mirrors function of
     * L::LogicalPartition) */
    [[nodiscard]] auto
    get_index_partition() const -> typename IndexSpec::index_partition_t {
      return
        typename IndexSpec::index_partition_t{m_handle.get_index_partition()};
    }

    /*! \brief partition's field space (mirrors function of L::LogicalPartition)
     */
    [[nodiscard]] auto
    get_field_space() const -> L::FieldSpace {
      return m_handle.get_field_space();
    }

    /*! \brief partition's tree id (mirrors function of L::LogicalPartition) */
    [[nodiscard]] auto
    get_tree_id() const -> L::RegionTreeID {
      return m_handle.get_tree_id();
    }

    /*! \brief partition existence test (mirrors function of
     * L::LogicalPartition)
     */
    [[nodiscard]] auto
    exists() const -> bool {
      return m_handle.exists();
    }

    /*! \brief partition's type tag (mirrors function of L::LogicalPartition) */
    [[nodiscard]] auto
    get_type_tag() const -> L::TypeTag {
      return m_handle.get_type_tag();
    }

    /*! \brief partition's dimension (mirrors function of L::LogicalPartition)
     */
    [[nodiscard]] auto
    get_dim() const -> int {
      return m_handle.get_dim();
    }
  };

  /* \brief create field space for these regions
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   *
   * \return FieldSpace with fields defined by the region
   */
  static auto
  field_space(L::Context ctx, L::Runtime* rt) -> L::FieldSpace {
    auto result = rt->create_field_space(ctx);
    auto fa = rt->create_field_allocator(ctx, result);
    (fa.allocate_field(
       sizeof(typename Fields::value_t),
       Fields::field_id,
       (Fields::serdez_id != nullptr ? *Fields::serdez_id : 0)),
     ...);
    return result;
  }

  /* \brief create index space for these regions with values for dynamic bounds
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   * \param dynamic_bounds bounds for non-statically defined dimensions of the
   * index space
   *
   * \return IndexSpec::index_space_t with well-defined bounds on all axes,
   * unless one or more dimensions would be empty or unbounded
   */
  static auto
  index_space(
    L::Context ctx, L::Runtime* rt, dynamic_bounds_t const& dynamic_bounds = {})
    -> std::optional<typename IndexSpec::index_space_t> {

    auto maybe_bounds = fix_bounds(dynamic_bounds);
    if (maybe_bounds)
      return index_space(ctx, rt, *maybe_bounds);
    return std::nullopt;
  }

  /*! \brief form of add_field() with parameter pack of field types */
  template <typename... Fs>
  static auto
  add_fields(L::RegionRequirement& req, Fs&&... fs) -> void {
    (StaticFieldMixin<IndexSpec, Fields...>::add_field(
       req, std::forward<Fs>(fs)),
     ...);
  }

  template <auto ColorDim>
  static auto
  create_partition_by_slicing(
    L::Context ctx,
    L::Runtime* rt,
    IndexSpec::index_space_t parent,
    std::tuple<
      typename IndexSpec::axes_t,
      typename IndexSpec::coord_t,
      typename IndexSpec::coord_t> const (&blocking_factors)[ColorDim],
    L::Color color = LEGION_AUTO_GENERATE_ID,
    const char* provenance = nullptr) -> IndexSpec::index_partition_t {

    using axes_t = typename IndexSpec::axes_t;
    using coord_t = typename IndexSpec::coord_t;

    auto find_bf = [&](auto&& ax) {
      return std::ranges::find_if(blocking_factors, [&](auto&& bf) {
        return std::get<0>(bf) == static_cast<axes_t>(ax);
      });
    };

    L::Transform<IndexSpec::dim, ColorDim, coord_t> transform;
    typename IndexSpec::rect_t extent =
      rt->get_index_space_domain(parent).bounds;
    L::Rect<ColorDim, coord_t> colors;
    colors.lo = L::Point<ColorDim, coord_t>::ZEROES();
    colors.hi = colors.lo;
    for (int i = 0; i < IndexSpec::dim; ++i) {
      transform[i] = L::Point<ColorDim, coord_t>::ZEROES();
      auto bf = find_bf(i);
      if (bf != &blocking_factors[ColorDim]) {
        auto bf_idx = std::distance(&blocking_factors[0], bf);
        auto& blk_sz = std::get<1>(*bf);
        transform[i][bf_idx] = blk_sz;
        auto extent_sz =
          std::make_unsigned_t<coord_t>(extent.hi[i] - extent.lo[i]) + 1;
        colors.lo[bf_idx] = std::get<2>(*bf);
        colors.hi[bf_idx] =
          rcp::ceil_div(extent_sz, std::make_unsigned_t<coord_t>(blk_sz))
          + colors.lo[bf_idx] - 1;
        extent.lo[i] -= blk_sz * colors.lo[bf_idx];
        extent.hi[i] = extent.lo[i] + blk_sz - 1;
      }
    }
    return rt
      ->create_partition_by_restriction<IndexSpec::dim, ColorDim, coord_t>(
        ctx,
        parent,
        rt->create_index_space(ctx, colors),
        transform,
        extent,
        LEGION_DISJOINT_COMPLETE_KIND,
        color,
        provenance);
  }

  template <auto ColorDim>
  static auto
  create_partition_by_slicing(
    L::Context ctx,
    L::Runtime* rt,
    IndexSpec::index_space_t parent,
    std::tuple<typename IndexSpec::axes_t, typename IndexSpec::coord_t> const (
      &blocking_factors)[ColorDim],
    L::Color color = LEGION_AUTO_GENERATE_ID,
    const char* provenance = nullptr) -> IndexSpec::index_partition_t {

    using axes_t = typename IndexSpec::axes_t;
    using coord_t = typename IndexSpec::coord_t;

    std::tuple<axes_t, coord_t, coord_t> blocks_and_offsets[ColorDim];
    for (std::size_t color = 0; color < ColorDim; ++color)
      blocks_and_offsets[color] = {
        std::get<0>(blocking_factors[color]),
        std::get<1>(blocking_factors[color]),
        0};

    return create_partition_by_slicing(
      ctx, rt, parent, blocks_and_offsets, color, provenance);
  }

  /*! \brief intersect dynamic and static bounds
   *
   * \param dynamic_bounds bounds for non-statically defined dimensions of the
   * index space
   *
   * \return array of bounds for all dimensions of the index space, unless one
   * or more dimensions would be unbounded or empty (in which case std::nullopt
   * is returned)
   */
  static auto
  fix_bounds(dynamic_bounds_t const& dynamic_bounds)
    -> std::optional<static_bounds_t> {

    auto result = StaticBounds;
    for (auto&& bnd : result) {
      auto idx = std::distance(result.begin(), &bnd);
      auto ax = static_cast<typename IndexSpec::axes_t>(idx);
      if (dynamic_bounds.contains(ax)) {
        auto maybe_intersection = bnd.intersection(dynamic_bounds.at(ax));
        if (maybe_intersection && maybe_intersection->is_bounded())
          bnd = *maybe_intersection;
        else
          return std::nullopt;
      }
    }
    return result;
  }

  /* \brief create index space for these regions with all bounds specified
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   * \param bounds bounds for all dimensions of the index space
   *
   * \return IndexSpec::index_space_t with well-defined bounds on all axes
   */
  static auto
  index_space(L::Context ctx, L::Runtime* rt, static_bounds_t const& bounds)
    -> IndexSpec::index_space_t {

    auto rect = typename IndexSpec::rect_t{};
    for (auto&& bnd : bounds) {
      auto idx = std::distance(bounds.begin(), &bnd);
      rect.lo[idx] = bnd.left();
      rect.hi[idx] = bnd.right();
    }
    return rt->create_index_space(ctx, rect);
  }
};

/*! \brief generator for dense array regions
 *
 * \tparam Spec specification for a partially define dense array region
 * (specialization of PartialStaticRegionSpec)
 */
template <typename Spec>
class DenseArrayRegionGenerator {
  /*! \brief index space */
  typename Spec::index_space_t m_is{};

  /*! \brief field space for regions */
  L::FieldSpace m_fs{};

public:
  using spec_t = Spec;

  /*! \brief default constructor (defaulted) */
  DenseArrayRegionGenerator() = default;
  /*! \brief constructor */
  DenseArrayRegionGenerator(
    L::Context ctx,
    L::Runtime* rt,
    typename spec_t::dynamic_bounds_t const& dynamic_bounds)
    : m_is(
      spec_t::index_space(ctx, rt, spec_t::fix_bounds(dynamic_bounds).value()))
    , m_fs(spec_t::field_space(ctx, rt)) {}
  /*! \brief copy constructor (deleted) */
  DenseArrayRegionGenerator(DenseArrayRegionGenerator const&) = delete;
  /*! \brief move constructor (defaulted) */
  DenseArrayRegionGenerator(DenseArrayRegionGenerator&&) noexcept = default;
  /*! \brief destructor (defaulted) */
  ~DenseArrayRegionGenerator() = default;
  /*! \brief copy assignment operator (deleted) */
  auto
  operator=(DenseArrayRegionGenerator const&)
    -> DenseArrayRegionGenerator& = delete;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(DenseArrayRegionGenerator&&) noexcept
    -> DenseArrayRegionGenerator& = default;

  /*! \brief destroy resources maintained by the instance */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void {
    if (m_fs != L::FieldSpace::NO_SPACE)
      rt->destroy_field_space(ctx, m_fs);
    if (m_is != typename spec_t::index_space_t{})
      rt->destroy_index_space(ctx, m_is);
  }

  /*! \brief make a logical region instance
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   *
   * \return IndexSpec::logical_region_t instance
   */
  auto
  make(L::Context ctx, L::Runtime* rt) -> typename spec_t::LogicalRegion {

    return typename spec_t::LogicalRegion{
      rt->create_logical_region(ctx, m_is, m_fs)};
  }

  [[nodiscard]] auto
  index_space() const -> typename spec_t::index_space_t {
    return m_is;
  }

  [[nodiscard]] auto
  field_space() const -> L::FieldSpace {
    return m_fs;
  }
};

/*! \brief generator for dynamic dense array regions
 *
 * \tparam Spec specification for a partially define dense array region
 * (specialization of PartialStaticRegionSpec)
 */
template <typename Spec>
class DynamicDenseArrayRegionGenerator {

  /*! \brief index space cache maximum size */
  std::size_t m_index_space_cache_max_size;
  /*! \brief index space cache for regions, indexed by region bounds */
  std::deque<
    std::tuple<typename Spec::static_bounds_t, typename Spec::index_space_t>>
    m_index_space_cache{};
  /*! \brief field space for regions */
  L::FieldSpace m_fs{};

public:
  using spec_t = Spec;

  /*! \brief constructor
   *
   * \param index_space_cache_max_size maximum size of index space cache
   */
  DynamicDenseArrayRegionGenerator(std::size_t index_space_cache_max_size = 1)
    : m_index_space_cache_max_size(index_space_cache_max_size) {
    assert(m_index_space_cache_max_size > 0);
  }
  /*! \brief copy constructor (deleted) */
  DynamicDenseArrayRegionGenerator(DynamicDenseArrayRegionGenerator const&) =
    delete;
  /*! \brief move constructor (defaulted) */
  DynamicDenseArrayRegionGenerator(
    DynamicDenseArrayRegionGenerator&&) noexcept = default;
  /*! \brief destructor (defaulted) */
  ~DynamicDenseArrayRegionGenerator() = default;
  /*! \brief copy assignment operator (deleted) */
  auto
  operator=(DynamicDenseArrayRegionGenerator const&)
    -> DynamicDenseArrayRegionGenerator& = delete;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(DynamicDenseArrayRegionGenerator&&) noexcept
    -> DynamicDenseArrayRegionGenerator& = default;

  /*! \brief destroy resources maintained by the instance */
  auto
  destroy(L::Context ctx, L::Runtime* rt) -> void {
    if (m_fs == L::FieldSpace::NO_SPACE)
      rt->destroy_field_space(ctx, m_fs);
    m_fs = {};
    for (auto&& is : m_index_space_cache)
      rt->destroy_index_space(ctx, std::get<1>(is));
    m_index_space_cache.clear();
  }

  /*! \brief make a logical region instance with values for dynamic bounds
   *
   * \param ctx Legion Context
   * \param rt Legion Runtime
   * \param dynamic_bounds bounds for non-statically defined dimensions of the
   * index space
   *
   * \return IndexSpec::logical_region_t instance, unless one or more dimensions
   * would be empty or unbounded
   */
  auto
  make(
    L::Context ctx,
    L::Runtime* rt,
    typename Spec::dynamic_bounds_t const& dynamic_bounds = {})
    -> std::optional<typename Spec::LogicalRegion> {

    if (m_fs == L::FieldSpace::NO_SPACE)
      m_fs = Spec::field_space(ctx, rt);

    auto maybe_is = index_space(ctx, rt, dynamic_bounds);
    if (maybe_is)
      return typename Spec::LogicalRegion{
        rt->create_logical_region(ctx, *maybe_is, m_fs)};
    return std::nullopt;
  }

  /*! \brief initialize index space cache
   *
   * \param ctx L::Context
   * \param rt L::Runtime
   * \param dynamic_bounds vector of dynamic bounds used to initialize the cache
   *
   * \return vector of index spaces in cache
   */
  auto
  init_index_space_cache(
    L::Context ctx,
    L::Runtime* rt,
    std::vector<typename Spec::dynamic_bounds_t> const& dynamic_bounds)
    -> void {

    for (auto&& dyn : dynamic_bounds)
      index_space(ctx, rt, dyn);
  }

  /*! \brief get copy of index space cache
   *
   * \return vector of index spaces in cache
   */
  auto
  index_space(
    L::Context ctx,
    L::Runtime* rt,
    typename Spec::dynamic_bounds_t const& dynamic_bounds)
    -> std::optional<typename Spec::index_space_t> {

    auto maybe_bounds = Spec::fix_bounds(dynamic_bounds);
    if (!maybe_bounds)
      return std::nullopt;

    auto is_match =
      std::ranges::find_if(m_index_space_cache, [&](auto&& bounds_is) {
        return std::get<0>(bounds_is) == *maybe_bounds;
      });
    typename Spec::index_space_t result;
    if (is_match != m_index_space_cache.end()) {
      result = std::get<1>(*is_match);
    } else {
      if (m_index_space_cache.size() == m_index_space_cache_max_size) {
        rt->destroy_index_space(ctx, std::get<1>(m_index_space_cache.back()));
        m_index_space_cache.pop_back();
      }
      result = Spec::index_space(ctx, rt, *maybe_bounds);
      m_index_space_cache.push_front({*maybe_bounds, result});
    }
    return result;
  }
};

/*! \brief wrapper class for a region and its parent
 *
 * \tparam T LogicalRegion type
 *
 * For use as type for LogicalRegion arguments passed to task launcher class
 * methods, to provide both the region and a parent region from which privileges
 * are derived.
 *
 * Can also be used for convenience within task implementations, from values in
 * the L::Task::regions[] array.
 */
template <typename T>
class RegionPair {
  T m_region{}; /*!< target region */
  T m_parent{}; /*!< parent region */

public:
  /*! \brief default constructor (defaulted) */
  RegionPair() = default;
  /*! \brief constructor from target region alone */
  RegionPair(T const& region) : m_region(region) {}
  /*! \brief constructor from target and parent regions */
  RegionPair(T const& region, T const& parent)
    : m_region(region), m_parent(parent) {}
  /*! \brief constructor from RegionRequirement
   *
   * Intended for use from within task implementations.
   *
   * This isn't entirely type safe, as it involves the construction of values of
   * type T, which will succeed for any LogicalRegion value that can be
   * converted to that type (i.e, matching L::LogicalRegionT<Dim, Coord>).
   */
  RegionPair(L::RegionRequirement const& req)
    : m_region(req.region), m_parent(req.parent) {}
  /*! \brief copy constructor (defaulted) */
  RegionPair(RegionPair const&) = default;
  /*! \brief move constructor (defaulted) */
  RegionPair(RegionPair&&) noexcept = default;
  /*! \brief destructor (defaulted) */
  ~RegionPair() = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(RegionPair const&) -> RegionPair& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(RegionPair&&) noexcept -> RegionPair& = default;

  /*! \brief target region handle */
  [[nodiscard]] auto
  region() const -> T {
    return m_region;
  }

  /*! \brief parent region handle
   *
   * \return parent region if one was provided to the constructor; otherwise,
   * the target region itself
   */
  [[nodiscard]] auto
  parent() const -> T {
    return (m_parent != T{}) ? m_parent : m_region;
  }

  template <IsStaticField... Fs>
  [[nodiscard]] auto
  requirement(
    std::variant<L::PrivilegeMode, L::ReductionOpID> priv_or_redop,
    L::CoherenceProperty coherence,
    StaticFields<Fs...>&& fields,
    std::optional<L::ProjectionID> const& proj = std::nullopt,
    L::MappingTagID tag = 0) const -> L::RegionRequirement {

    return region().requirement(
      priv_or_redop, coherence, std::move(fields), proj, parent(), tag);
  }
};

/*! \brief wrapper class for a partition and its parent
 *
 * \tparam T LogicalPartition type
 *
 * For use as type for LogicalPartition arguments passed to task launcher class
 * methods, to provide both the partition and a parent region from which
 * privileges are derived.
 */
template <typename T>
class PartitionPair {
  T m_partition{};                 /*!< partition */
  typename T::region_t m_parent{}; /*!< parent region */

public:
  using region_t = typename T::region_t;

  /*! \brief default constructor (defaulted) */
  PartitionPair() = default;
  /*! \brief constructor from partition alone */
  PartitionPair(T const& partition) : m_partition(partition) {}
  /*! \brief constructor from partition and parent regions */
  PartitionPair(T const& partition, typename T::region_t const& parent)
    : m_partition(partition), m_parent(parent) {}
  /*! \brief copy constructor (defaulted) */
  PartitionPair(PartitionPair const&) = default;
  /*! \brief move constructor (defaulted) */
  PartitionPair(PartitionPair&&) noexcept = default;
  /*! \brief destructor (defaulted) */
  ~PartitionPair() = default;
  /*! \brief copy assignment operator (defaulted) */
  auto
  operator=(PartitionPair const&) -> PartitionPair& = default;
  /*! \brief move assignment operator (defaulted) */
  auto
  operator=(PartitionPair&&) noexcept -> PartitionPair& = default;

  /*! \brief partition handle */
  [[nodiscard]] auto
  partition() const -> T {
    return m_partition;
  }

  /*! \brief parent region handle
   *
   * \return parent region if one was provided to the constructor; otherwise,
   * the
   */
  [[nodiscard]] auto
  parent(L::Runtime* rt) const -> region_t {
    return (m_parent != region_t{})
             ? m_parent
             : region_t{rt->get_parent_logical_region(
               typename T::index_spec_t::logical_partition_t{m_partition})};
  }

  template <IsStaticField... Fs>
  [[nodiscard]] auto
  requirement(
    L::Runtime* rt,
    L::ProjectionID proj,
    std::variant<L::PrivilegeMode, L::ReductionOpID> priv_or_redop,
    L::CoherenceProperty coherence,
    StaticFields<Fs...>&& fields,
    L::MappingTagID tag = 0) const -> L::RegionRequirement {

    return partition().requirement(
      proj, priv_or_redop, coherence, parent(rt), std::move(fields), tag);
  }
};

} // end namespace rcp::rcpl

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
