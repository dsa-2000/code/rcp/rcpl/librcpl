// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp/rcpl/rcpl.hpp"

#include <chrono>
#include <ostream>

namespace rcp::rcpl {

struct RCPL_EXPORT DataPacketConfiguration {

  /*! number of channels per F-engine packet */
  static constexpr unsigned num_channels_per_packet =
    RCP_NUM_CHANNELS_PER_PACKET;

  /*! number of timesteps per F-engine packet */
  static constexpr unsigned num_timesteps_per_packet =
    RCP_NUM_TIMESTEPS_PER_PACKET;

  /*! timesteps tile size
   *
   * value of 0 corresponds to no tiling
   */
  static constexpr unsigned timesteps_tile_size =
    RCP_PACKET_TIMESTEPS_TILE_SIZE;

  static_assert(
    timesteps_tile_size == 0
    || num_timesteps_per_packet % timesteps_tile_size == 0);

  /*! number of polarizations per channel from F-engine */
  static constexpr unsigned num_polarizations = RCP_NUM_POLARIZATIONS;

  /*! number of receivers (antennas) from F-engines */
  static constexpr unsigned num_receivers = RCP_NUM_RECEIVERS;

  /*! duration type for a fsample
   */
  using fsample_duration_t = std::chrono::duration<
    std::int64_t,
    std::ratio_multiply<std::ratio<RCP_SAMPLE_INTERVAL_NS, 1>, std::nano>>;
  static constexpr fsample_duration_t fsample_interval{1};
};

} // end namespace rcp::rcpl

namespace std {

// NOLINTBEGIN(cert-dcl58-cpp)
RCPL_EXPORT auto
operator<<(std::ostream&, rcp::rcpl::DataPacketConfiguration const&)
  -> std::ostream&;
// NOLINTEND(cert-dcl58-cpp)
} // end namespace std

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
