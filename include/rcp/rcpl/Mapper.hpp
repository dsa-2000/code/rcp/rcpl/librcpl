// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcpl.hpp"

#include <bit>
#include <mappers/default_mapper.h>

namespace rcp::rcpl {

/*! \brief mapper class for rcpl */
class RCPL_EXPORT Mapper : public L::Mapping::DefaultMapper {

  enum : unsigned {
    loc_restricted = 0,
    omp_restricted,
    toc_restricted,
    py_restricted,
    io_restricted,
    num_proc_restricted
  };

  enum : unsigned {
    level0_sharding_tag_shift = 8,
    high_priority_tag_shift,
    // collective_instance_tag_shift, TODO: remove permanently
    proc_restricted_tag_shift,
    next_tag_shift =
      proc_restricted_tag_shift
      + std::bit_width(static_cast<unsigned>(num_proc_restricted))
  };

public:
  static constexpr L::MappingTagID level0_sharding_tag =
    (1U << level0_sharding_tag_shift);
  static constexpr L::MappingTagID high_priority_tag =
    (1U << high_priority_tag_shift);
  // static constexpr L::MappingTagID collective_instance_tag =
  //   (1U << collective_instance_tag_shift);
  static constexpr L::MappingTagID loc_restricted_tag =
    (loc_restricted + 1) << proc_restricted_tag_shift;
  static constexpr L::MappingTagID omp_restricted_tag =
    (omp_restricted + 1) << proc_restricted_tag_shift;
  static constexpr L::MappingTagID toc_restricted_tag =
    (toc_restricted + 1) << proc_restricted_tag_shift;
  static constexpr L::MappingTagID py_restricted_tag =
    (py_restricted + 1) << proc_restricted_tag_shift;
  static constexpr L::MappingTagID io_restricted_tag =
    (io_restricted + 1) << proc_restricted_tag_shift;

  static L::TaskPriority constexpr high_priority_value = 100;

  static auto
  restricted_proc_kind(L::MappingTagID tag) -> L::Processor::Kind;

  Mapper(
    L::Machine const& machine,
    L::Runtime* rt,
    std::set<L::Processor> const& local_procs);

  auto
  select_task_options(
    L::Mapping::MapperContext ctx,
    L::Task const& task,
    L::Mapping::Mapper::TaskOptions& options) -> void override;

  auto
  map_task(
    L::Mapping::MapperContext ctx,
    L::Task const& task,
    L::Mapping::Mapper::MapTaskInput const& input,
    L::Mapping::Mapper::MapTaskOutput& output) -> void override;

  using L::Mapping::DefaultMapper::select_sharding_functor;

  auto
  select_sharding_functor(
    L::Mapping::MapperContext ctx,
    L::Task const& task,
    L::Mapping::Mapper::SelectShardingFunctorInput const& input,
    L::Mapping::Mapper::SelectShardingFunctorOutput& output) -> void override;

  auto
  default_policy_select_initial_processor(
    L::Mapping::MapperContext ctx, const L::Task& task)
    -> L::Processor override;

  auto
  create_custom_instances(
    L::Mapping::MapperContext ctx,
    L::Processor target_proc,
    L::Memory target_memory,
    L::RegionRequirement const& req,
    unsigned index,
    std::set<L::FieldID>& needed_fields,
    L::TaskLayoutConstraintSet const& layout_constraints,
    bool needs_field_constraint_check,
    std::vector<L::Mapping::PhysicalInstance>& instances,
    size_t* footprint = nullptr) -> bool;

  static auto
  create_mappers(
    L::Machine machine,
    L::Runtime* rt,
    const std::set<L::Processor>& local_procs) -> void;

  static auto
  preregister() -> void;
};

} // end namespace rcp::rcpl

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
