// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp/rcpl/rcpl.hpp"

#include <chrono>
#include <vector>

namespace rcp::rcpl {

/** get current system time
 */
template <L::TaskID TaskId>
class GetEpoch_ {

  static constexpr auto tid = TaskId;

  L::TaskLauncher m_launcher;

public:
  using value_t = std::chrono::system_clock::time_point;

  GetEpoch_() : m_launcher(tid, L::UntypedBuffer()) {}

  auto
  launch(L::Context ctx, L::Runtime* rt) -> L::Future {

    return rt->execute_task(ctx, m_launcher);
  }

  static auto
  task(
    L::Task const* /*task*/,
    std::vector<L::PhysicalRegion> const& /*regions*/,
    L::Context /*ctx*/,
    L::Runtime* /*rt*/) -> value_t {

    // TODO: use a different clock? e.g, gps_clock or tai_clock
    // these alternatives appear undefined with gcc 12.1
    return std::chrono::system_clock::now();
  }

  static void
  preregister() {
    L::TaskVariantRegistrar registrar(tid, "GetEpoch variant");
    registrar.add_constraint(L::ProcessorConstraint(L::Processor::LOC_PROC));
    registrar.set_leaf();
    L::Runtime::preregister_task_variant<value_t, task>(
      registrar, "GetEpoch task");
  }
};
} // end namespace rcp::rcpl

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
